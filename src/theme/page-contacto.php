<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 */

get_header(); ?>

<?php 
	
	// Get content width and sidebar position
	$content_class = woodmart_get_content_class();

?>

<div class="site-content <?php echo esc_attr( $content_class ); ?>" role="main">
    <?php /* The loop */ ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="entry-content">
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="title">
                            <h2>Contáctanos</h2>
                        </div>
                        <?php
                        // Recupera el nombre del producto del que se requiere una cotización, enviado desde un formulario del producto en cuestión
                        // Se crea una elemento oculto con el valor de la variable para después insertarlo en el campo Asunto del formulario mediante jQuery
                        $producto = "";
                        if (isset($_POST['producto'])&&$_POST['producto']) {
                            $producto = $_POST['producto'];
                        };
    
                        ?>      
                        <form novalidate class="salesforce-form" id="desego-contact-form" method="POST">

                            <input type=hidden name="oid" value="00D1a000000JmNP">
                            <input type=hidden name="retURL" value="<?php echo site_url(); ?>/contacto/">
                            <input type=hidden name="action" value="desego_contact_form">
                            <input type="hidden" id="lead_source" name="lead_source" value="Sucursal virtual" />

                            <!--  NOTA: Estos campos son elementos de depuración opcionales. Elimine      -->
                            <!--  los comentarios de estas líneas si desea realizar una prueba en el      -->
                            <!--  modo de depuración.                                                     -->
                            <!--  <input type="hidden" name="debug" value=1>                              -->
                            <!--  <input type="hidden" name="debugEmail" value="ventas@desego.com">       -->

                            <p>
                                <input  id="00N1a000003RnzJ" name="00N1a000003RnzJ" type="text" placeholder="Asunto*" value="<?= $producto; ?>" />
                            </p>

                            <p>
                                <input id="first_name" name="first_name" type="text" placeholder="Nombre*" />
                            </p>
                            
                            <p>
                                <input id="last_name" name="last_name" type="text" placeholder="Apellidos*" />
                            </p>
                            
                            <p>
                                <input id="phone" name="phone" type="text" placeholder="Teléfono*" />
                            </p>
                            
                            <p>
                                <input id="mobile" name="mobile" type="text" placeholder="Celular" />
                            </p>
                            
                            <p>
                                <input id="email" name="email" type="text" placeholder="Email*" />
                            </p>
                            
                            <p>
                                <input id="00N1a000003ZMlP" name="00N1a000003ZMlP" type="text" placeholder="Empresa/Laboratorio" />
                            </p>
                            
                            

                            <p>
                                <select id="country_code" name="country_code" title="País">
                                    <option value="">--País--</option>
                                    <option value="AR">Argentina</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BO">Bolivia, Plurinational State of</option>
                                    <option value="BR">Brazil</option>
                                    <option value="CA">Canada</option>
                                    <option value="CL">Chile</option>
                                    <option value="CO">Colombia</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="HN">Honduras</option>
                                    <option value="MX">Mexico</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="PA">Panama</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>
                                    <option value="US">United States</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="VE">Venezuela, Bolivarian Republic of</option>
                                </select>
                            </p>

                            <p>
                                <select id="state_code" name="state_code" title="Estado/Provincia">
                                    <option value="">--Estado/Provincia--</option>
                                    <option value="ZZ">.Otro</option>
                                    <option value="AG">Aguascalientes</option>
                                    <option value="BC">Baja California</option>
                                    <option value="BS">Baja California Sur</option>
                                    <option value="CM">Campeche</option>
                                    <option value="CS">Chiapas</option>
                                    <option value="CH">Chihuahua</option>
                                    <option value="CO">Coahuila</option>
                                    <option value="CL">Colima</option>
                                    <option value="DG">Durango</option>
                                    <option value="DF">Federal District</option>
                                    <option value="GT">Guanajuato</option>
                                    <option value="GR">Guerrero</option>
                                    <option value="HG">Hidalgo</option>
                                    <option value="JA">Jalisco</option>
                                    <option value="ME">Mexico State</option>
                                    <option value="MI">Michoacán</option>
                                    <option value="MO">Morelos</option>
                                    <option value="NA">Nayarit</option>
                                    <option value="NL">Nuevo León</option>
                                    <option value="OA">Oaxaca</option>
                                    <option value="PB">Puebla</option>
                                    <option value="QE">Querétaro</option>
                                    <option value="QR">Quintana Roo</option>
                                    <option value="SL">San Luis Potosí</option>
                                    <option value="SI">Sinaloa</option>
                                    <option value="SO">Sonora</option>
                                    <option value="TB">Tabasco</option>
                                    <option value="TM">Tamaulipas</option>
                                    <option value="TL">Tlaxcala</option>
                                    <option value="VE">Veracruz</option>
                                    <option value="YU">Yucatán</option>
                                    <option value="ZA">Zacatecas</option>
                                </select>
                            </p>

                            <p>
                                <select id="sector" name="sector" title="Sector">
                                    <option value="">--Sector--</option>
                                    <option value="Humano">Humano</option>
                                    <option value="Veterinario">Veterinario</option>
                                    <option value="Patología">Patología</option>
                                </select>
                            </p>

                            <p>
                                <textarea id="00N1a000002vRer" name="00N1a000002vRer" placeholder="Comentarios"></textarea>
                            </p>
                            
                            <p>
                                <input type="submit" name="submit" class="button" />
                                <img class="ajax-loader" src="<?php echo content_url(); ?>/plugins/contact-form-7/images/ajax-loader.gif" alt="Enviando..." style="display: none;">
                            </p>

                            <div class='contact-form-notif wpcf7-response-output wpcf7-display-none'></div>

                        </form>
                        <script>
                            (function ($) {
                                var $desegoContactForm = $('#desego-contact-form');
                                var $formNotification = $('.contact-form-notif');
                                var $ajaxLoader = $('.ajax-loader');
                                $desegoContactForm.on('submit', function(e){
                                    e.preventDefault();
                                    var data = $desegoContactForm.serialize();
                                    $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: '<?php echo admin_url('admin-ajax.php') ?>',
                                        data: data,
                                        beforeSend: function(){
                                            $('.wpcf7-not-valid-tip').remove();
                                            $formNotification.empty().removeClass('wpcf7-validation-errors wpcf7-mail-sent-ok hidden-notice').addClass('wpcf7-display-none');
                                            $ajaxLoader.show();
                                        },
                                        success: function(response) {
                                            $ajaxLoader.hide();
                                            if(!response.success){
                                                if(typeof response.data === 'undefined' || typeof response.data.errors === 'undefined'){
                                                    console.log(response);
                                                    $formNotification.text('Ha ocurrido un error, favor de intentar más tarde o contactar al administrador del sitio.');
                                                }else{
                                                    $formNotification.text('Uno o más campos tienen un error. Por favor revisa e inténtalo de nuevo.');
                                                    
                                                    $.each(response.data.errors, function(key, value){
                                                        html_error = '<span class="wpcf7-not-valid-tip">'+value+'</span>';
                                                        $target=$('#'+key)
                                                        $target.after(html_error);
                                                    });
                                                }
                                                $formNotification.addClass('wpcf7-validation-errors');
                                            }else if(response.success){
                                                $formNotification.addClass('wpcf7-mail-sent-ok').text('Su mensaje se ha enviado correctamente, nos pondremos en contacto a la brevedad. Gracias.');
                                                $desegoContactForm.trigger('reset');
                                            }
                                            $formNotification.removeClass('wpcf7-display-none');
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            console.log(xhr.status);
                                            console.log(thrownError);
                                        }
                                    });
                                    
                                });
                            })(jQuery);
                        </script>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <?php the_content(); ?>
                    </div>
                </div>
                <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'woodmart' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
            </div>
            <?php woodmart_entry_meta(); ?>
        </article><!-- #post -->
        <?php 
        // If comments are open or we have at least one comment, load up the comment template.
        if ( woodmart_get_opt('page_comments') && (comments_open() || get_comments_number()) ) :
            comments_template();
        endif;
        ?>
    <?php endwhile; ?>

</div><!-- .site-content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
