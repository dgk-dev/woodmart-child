<?php
// INCLUDES
//resources: CSS JS FONTS
require(get_stylesheet_directory().'/includes/resources.php');

//store changes. From desego v3
require(get_stylesheet_directory().'/includes/store-changes.php');

// Custom post type: insertos
require(get_stylesheet_directory().'/includes/custom-post-type-insertos.php');

// Funciones de mis equipos: registro de páginas "mis equipos" en woocommerce y carga con ajax
require(get_stylesheet_directory().'/includes/equipment.php');

// Cambios y ajustes del sistema de puntos
require(get_stylesheet_directory().'/includes/rewardsystem.php');

// Google analytics
require(get_stylesheet_directory().'/includes/g-analytics.php');

// widget de rastreo
require(get_stylesheet_directory().'/includes/widgets/tracking-widget.php');

// widget de lista de paqueterías
require(get_stylesheet_directory().'/includes/widgets/tracking-list-widget.php');

// widget de lista de tiendas para bajar APP
require(get_stylesheet_directory().'/includes/widgets/app-list-widget.php');

// métodos de formulario de contacto
require(get_stylesheet_directory().'/includes/contact-form.php');

// métodos de formulario de soporte
require(get_stylesheet_directory().'/includes/support-form.php');

// woodmart template changes/overwrites
require(get_stylesheet_directory().'/includes/woodmart-changes.php');

// chat
require(get_stylesheet_directory().'/includes/chat.php');


//langs: themes and plugins
add_action( 'after_setup_theme', 'woodmartchild_theme_lang' );
function woodmartchild_theme_lang() {
    load_child_theme_textdomain( 'woodmart', get_stylesheet_directory() . '/languages' );
    
    //plugins
    unload_textdomain('woocommerce-multiple-customer-addresses');
    load_textdomain('woocommerce-multiple-customer-addresses', get_stylesheet_directory() . '/languages/woocommerce-multiple-customer-addresses-es_ES.mo');
    
    unload_textdomain('rewardsystem');
    load_textdomain('rewardsystem', get_stylesheet_directory() . '/languages/rewardsystem-es_ES.mo');

}

//excerpt to 40 words
add_filter( 'excerpt_length', 'desego_custom_excerpt_length', 999 );
function desego_custom_excerpt_length( $length ) {
    return 40;
}

// Disable woocommerce image regeneration
add_filter( 'woocommerce_background_image_regeneration', '__return_false', 99 );

// Disable password strength meter
add_action( 'wp_enqueue_scripts', 'desego_disable_password_strength_meter', 10 );
function desego_disable_password_strength_meter() {
    wp_dequeue_script( 'wc-password-strength-meter' );
}

// Change Password change email FROM to admin
add_filter('password_change_email', 'desego_password_change_email', 10, 3);
function desego_password_change_email($pass_change_email, $user, $userdata){
    $pass_change_email['headers'] = array('From: '.get_bloginfo('name').' <noreply@desego.com>');
    return $pass_change_email;
}

function year_shortcode() {
    $year = date('Y');
    return $year;
}
add_shortcode('year', 'year_shortcode');