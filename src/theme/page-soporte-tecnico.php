<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 */

get_header(); ?>

<?php

// Get content width and sidebar position
$content_class = woodmart_get_content_class();

//get user data
$user_id = get_current_user_id();
$user = get_user_meta($user_id);
?>

<div class="site-content <?php echo esc_attr($content_class); ?>" role="main">
    <?php /* The loop */ ?>
    <?php while (have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="entry-content">
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <?php
                        // Recupera el nombre del producto del que se requiere una cotización, enviado desde un formulario del producto en cuestión
                        // Se crea una elemento oculto con el valor de la variable para después insertarlo en el campo Asunto del formulario mediante jQuery
                        

                        ?>
                        <form novalidate class="salesforce-form" id="desego-support-form" method="POST">

                            <input type=hidden name="orgid" value="00D1a000000JmNP">
                            <input type=hidden name="retURL" value="<?php echo site_url(); ?>/soporte-tecnico/">
                            <input type="hidden" name="action" value="desego_support_form">

                            <!--  ----------------------------------------------------------------------  -->
                            <!--  NOTA: Estos campos son elementos de depuración opcionales. Elimine      -->
                            <!--  los comentarios de estas líneas si desea realizar una prueba en el      -->
                            <!--  modo de depuración.                                                     -->
                            <!--  <input type="hidden" name="debug" value=1>                              -->
                            <!--  <input type="hidden" name="debugEmail" value="ricardo@dgk.com.mx">      -->
                            <!--  ----------------------------------------------------------------------  -->

                            <p>
                                <select id="reason" name="reason">
                                    <option value="">--Motivo del caso--</option>
                                    <option value="Falla equipo">Falla equipo</option>
                                    <option value="Duda de funcionamiento/software">Duda de funcionamiento/software</option>
                                    <option value="Capacitación">Capacitación</option>
                                    <option value="Error en resultados">Error en resultados</option>
                                </select>
                            </p>

                            <p>
                                <input id="name" maxlength="80" name="name" size="20" type="text" placeholder="Nombre del contacto*" value="<?php echo isset($user['first_name']) ? $user['first_name'][0].' '.$user['last_name'][0] : '' ?>" />
                            </p>


                            <p>
                                <input id="phone" maxlength="10" name="phone" size="20" type="text"  placeholder="Teléfono*" value="<?php echo isset($user['billing_phone']) ? $user['billing_phone'][0] : '' ?>" />

                            </p>
                            <p>
                                <input id="email" maxlength="80" name="email" size="20" type="text" placeholder="Correo electrónico*" value="<?php echo isset($user['billing_email']) ? $user['billing_email'][0] : '' ?>" />
                            </p>

                            <p>
                                <select id="00N1a000008zTNe" name="00N1a000008zTNe" title="Equipo">
                                    <option value="">--Equipo*--</option>
                                    <option value="1H">1H</option>
                                    <option value="3H">3H</option>
                                    <option value="5H">5H</option>
                                    <option value="5Rvet">5Rvet</option>
                                    <option value="AE+">AE+</option>
                                    <option value="AE-22">AE-22</option>
                                    <option value="Anestesia">Anestesia</option>
                                    <option value="Autokem II">Autokem II</option>
                                    <option value="B 200">B 200</option>
                                    <option value="BC 3000">BC 3000</option>
                                    <option value="BC 7000">BC 7000</option>
                                    <option value="BC H+">BC H+</option>
                                    <option value="BC H5+">BC H5+</option>
                                    <option value="Bc Vet">Bc Vet</option>
                                    <option value="Bomba infusión">Bomba infusión</option>
                                    <option value="Campana">Campana</option>
                                    <option value="Centrífuga">Centrífuga</option>
                                    <option value="CK-12">CK-12</option>
                                    <option value="CK-18">CK-18</option>
                                    <option value="CK-24">CK-24</option>
                                    <option value="CK-6F">CK-6F</option>
                                    <option value="CK-6V">CK-6V</option>
                                    <option value="CK-8">CK-8</option>
                                    <option value="CL900i">CL900i</option>
                                    <option value="Digitalizador CR">Digitalizador CR</option>
                                    <option value="Digitalizador DR">Digitalizador DR</option>
                                    <option value="Easykem plus">Easykem plus</option>
                                    <option value="Easykem pro">Easykem pro</option>
                                    <option value="Easy vet">Easy vet</option>
                                    <option value="EC 100">EC 100</option>
                                    <option value="EC 300">EC 300</option>
                                    <option value="EK-25">EK-25</option>
                                    <option value="EK-36">EK-36</option>
                                    <option value="Ekem">Ekem</option>
                                    <option value="Eliread">Eliread</option>
                                    <option value="Eliwash">Eliwash</option>
                                    <option value="Equipo básico">Equipo básico</option>
                                    <option value="ES 218">ES 218</option>
                                    <option value="ES 221">ES 221</option>
                                    <option value="ES 300">ES 300</option>
                                    <option value="ES400">ES400</option>
                                    <option value="FIB 1000">FIB 1000</option>
                                    <option value="FIB 2000">FIB 2000</option>
                                    <option value="Fib II">Fib II</option>
                                    <option value="G11">G11</option>
                                    <option value="G8">G8</option>
                                    <option value="GX">GX</option>
                                    <option value="i15">i15</option>
                                    <option value="i15 vet">i15 vet</option>
                                    <option value="iChamber">iChamber</option>
                                    <option value="iChroma">iChroma</option>
                                    <option value="iChroma II">iChroma II</option>
                                    <option value="iKem">iKem</option>
                                    <option value="K10">K10</option>
                                    <option value="K20">K20</option>
                                    <option value="K3">K3</option>
                                    <option value="K6">K6</option>
                                    <option value="Limpiador bucal vet">Limpiador bucal vet</option>
                                    <option value="M8">M8</option>
                                    <option value="M8i">M8i</option>
                                    <option value="Microscopio">Microscopio</option>
                                    <option value="Mini Bc">Mini Bc</option>
                                    <option value="Mini BcVet">Mini BcVet</option>
                                    <option value="OX 100">OX 100</option>
                                    <option value="Petmap I">Petmap I</option>
                                    <option value="Petmap II">Petmap II</option>
                                    <option value="PL3">PL3</option>
                                    <option value="PL 4">PL 4</option>
                                    <option value="Pluslyte II">Pluslyte II</option>
                                    <option value="Quickvet">Quickvet</option>
                                    <option value="Rayos X">Rayos X</option>
                                    <option value="Rayos X con tablet">Rayos X con tablet</option>
                                    <option value="Skyla HB1">Skyla HB1</option>
                                    <option value="Skyla VB1">Skyla VB1</option>
                                    <option value="Super F M600D">Super F M600D</option>
                                    <option value="Termometro Parlante">Termometro Parlante</option>
                                    <option value="Uriplus">Uriplus</option>
                                    <option value="Uriplus II">Uriplus II</option>
                                    <option value="Vivolight">Vivolight</option>
                                    <option value="iM70">iM70</option>
                                </select>
                            </p>

                            <p>
                                <textarea id="description" name="description"  placeholder="Descripción*" ></textarea>
                            </p>

                            <p>
                                <input type="submit" name="submit" class="button" />
                                <img class="ajax-loader" src="<?php echo content_url(); ?>/plugins/contact-form-7/images/ajax-loader.gif" alt="Enviando..." style="display: none;">
                            </p>

                            <div class='contact-form-notif wpcf7-response-output wpcf7-display-none'></div>

                        </form>
                        <script>
                            (function($) {
                                var $desegoSupportForm = $('#desego-support-form');
                                var $formNotification = $('.contact-form-notif');
                                var $ajaxLoader = $('.ajax-loader');
                                $desegoSupportForm.on('submit', function(e) {
                                    e.preventDefault();
                                    var data = $desegoSupportForm.serialize();
                                    $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: '<?php echo admin_url('admin-ajax.php') ?>',
                                        data: data,
                                        beforeSend: function() {
                                            $('.wpcf7-not-valid-tip').remove();
                                            $formNotification.empty().removeClass('wpcf7-validation-errors wpcf7-mail-sent-ok hidden-notice').addClass('wpcf7-display-none');
                                            $ajaxLoader.show();
                                        },
                                        success: function(response) {
                                            $ajaxLoader.hide();
                                            if (!response.success) {
                                                if (typeof response.data === 'undefined' || typeof response.data.errors === 'undefined') {
                                                    console.log(response);
                                                    $formNotification.text('Ha ocurrido un error, favor de intentar más tarde o contactar al administrador del sitio.');
                                                } else {
                                                    $formNotification.text('Uno o más campos tienen un error. Por favor revisa e inténtalo de nuevo.');

                                                    $.each(response.data.errors, function(key, value) {
                                                        html_error = '<span class="wpcf7-not-valid-tip">' + value + '</span>';
                                                        $target = $('#' + key)
                                                        $target.after(html_error);
                                                    });
                                                }
                                                $formNotification.addClass('wpcf7-validation-errors');
                                            } else if (response.success) {
                                                $formNotification.addClass('wpcf7-mail-sent-ok').text('Su mensaje se ha enviado correctamente, nos pondremos en contacto a la brevedad. Gracias.');
                                                $desegoSupportForm.trigger('reset');
                                                console.log(response);
                                            }
                                            $formNotification.removeClass('wpcf7-display-none');
                                        },
                                        error: function(xhr, ajaxOptions, thrownError) {
                                            console.log(xhr.status);
                                            console.log(thrownError);
                                        }
                                    });

                                });


                            })(jQuery);
                        </script>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <?php the_content(); 
                        ?>
                    </div>
                </div>
                <?php wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . esc_html__('Pages:', 'woodmart') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>
            </div>
            <?php woodmart_entry_meta(); ?>
        </article><!-- #post -->
        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if (woodmart_get_opt('page_comments') && (comments_open() || get_comments_number())) :
            comments_template();
        endif;
        ?>
    <?php endwhile; ?>

</div><!-- .site-content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>