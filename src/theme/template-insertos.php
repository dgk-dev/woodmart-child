<?php
/*Template Name: Insertos*/
get_header();

?>

        
<aside class="sidebar-container col-lg-3 col-md-3 col-12 order-md-first sidebar-left area-sidebar-shop" role="complementary">
	<div class="widget-heading">
		<a href="#" class="close-side-widget">cerrar</a>
	</div>
	<?php
	$terms  = get_terms('insertos_categories');

	if (count($terms)) : ?>
			<div id="container-async" data-paged="-1" class="sc-ajax-filter sc-ajax-filter-multi widget_layered_nav sidebar-widget">
				<h5 class="widget-title">Categorías</h5>
				<ul class="nav-filter woocommerce-widget-layered-nav-list woodmart-widget">
					<li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term">
						<a href="#" data-filter="insertos_categories" data-term="all-terms" data-page="1">
							Todas
						</a>
					</li>
					<?php foreach ($terms as $term) : ?>
						<li>
							<a href="<?php echo get_term_link( $term, $term->taxonomy ); ?>" data-filter="<?php echo $term->taxonomy; ?>" data-term="<?php echo $term->slug; ?>" data-page="1">
								<?php echo $term->name; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		<?php endif; ?>
</aside>

<div class="site-content shop-content-area col-lg-9 col-12 col-md-9 order-md-last description-area-before content-with-products" role="main" style="min-height: 60vh;">
	<div class="woodmart-shop-tools">
		<div class="woodmart-show-sidebar-btn">
			<span class="woodmart-side-bar-icon"></span>
			<span>Mostrar filtros</span>
		</div>
	</div>
	<div id="insertos_filters" class="woodmart-shop-loader"></div>
	<div id="insertos_posts_wrap" class="insertos-wrap">
	<!-- Posts will be here -->
	</div>
</div><!-- /#main-content -->
<?php get_footer(); ?>