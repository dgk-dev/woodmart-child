<?php
function desego_resources(){
  $parent_style = 'woodmart-style';
  wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
  wp_enqueue_style( 'child-style',
    get_stylesheet_directory_uri() . '/style.css',
    array( $parent_style ),
    woodmart_get_theme_info( 'Version' )
  );

  wp_register_script( 'desego-functions', get_stylesheet_directory_uri() . '/js/footer-bundle.js', null, '1.0', true);
  wp_localize_script('desego-functions', 'desegoGlobalObject', array(
    'add_cart_button_script' => is_shop() || is_product_category() || is_product_tag(),
  ));
  wp_enqueue_script( 'desego-functions' );
}
add_action('wp_enqueue_scripts', 'desego_resources');

//quitar duplicados de font-awesome 4. Fontawesome 4 y 5 ya se encuentra compilados en style.css del child
function remove_fontawesome_duplicates(){
  wp_dequeue_style( 'font-awesome-css' );
  wp_deregister_style( 'font-awesome-css' );
  wp_dequeue_style( 'vc_animate-css' );
  wp_deregister_style( 'vc_animate-css' );
  wp_dequeue_style( 'vc_font_awesome_5_shims' );
  wp_deregister_style( 'vc_font_awesome_5_shims' );
  
  if ( defined( 'YITH_WCWL_URL' ) ) {
    wp_dequeue_style( 'yith-wcwl-font-awesome' );
    wp_deregister_style( 'yith-wcwl-font-awesome' );
    wp_deregister_style( 'yith-wcwl-main' );
    wp_enqueue_style( 'yith-wcwl-main', YITH_WCWL_URL . 'assets/css/style.css', array( 'jquery-selectBox'), YITH_WCWL_Init::get_instance()->version);
  }
}
add_action('wp_enqueue_scripts', 'remove_fontawesome_duplicates', 10001);
//Agregar datos para SEO
add_action('wp_head', 'desego_website_data', 99);
function desego_website_data() {
  $name = get_bloginfo( 'name' );
  $url = get_bloginfo( 'url' );
  ?>
  <script type="application/ld+json">
  {
    "@context" : "http://schema.org",
    "@type" : "Organization",
    "name" : "<?php echo $name ?>",
    "url" : "<?php echo $url ?>",
    "sameAs" : [
      "https://www.facebook.com/SOMOSDESEGO/",
      "https://www.youtube.com/channel/UCnjHTR-qQBvhQfUl7kCI_PA/",
      "https://www.linkedin.com/company/desego-equipo-de-laboratorio/"
    ],
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "Fuente de la Rana #58. Col. Fuentes de Morelia",
      "addressRegion": "Morelia, Michoacán",
      "postalCode": "58080",
      "addressCountry": "MX"
    }
  }
  </script>
  <?php
}
