<?php

//Crear tabla para equipos al activar el theme
add_action("after_switch_theme", "desego_create_equipment_table");

function desego_create_equipment_table(){
    if (isset($_GET['activated']) && is_admin()){
        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . "desego_equipment";
        
        $sql = "CREATE TABLE $table_name (
        id int(10) unsigned NOT NULL AUTO_INCREMENT,
        user_id int(10) NULL,
        area VARCHAR(255) NULL,
        equipment TEXT NULL,
        interests TEXT NULL,
        created VARCHAR(255) NULL,
        updated VARCHAR(255) NULL,
        PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
}

// Agrega las paginas (endpoints) en el apartado "mi cuenta" de woocommerce

//registra link en el menú de mi cuenta de woocommerce
function desego_account_menu_items( $items ) {
    $items['equipos'] = __( 'Mis equipos', 'woodmart' );
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'desego_account_menu_items', 10, 1 );


//Registra endpoints para que no haya paginas 404
function desego_add_my_account_endpoints() {
    add_rewrite_endpoint( 'equipmentDashboard', EP_PAGES );
    add_rewrite_endpoint( 'equipmentSelectarea', EP_PAGES );
    add_rewrite_endpoint( 'equipmentSelectequipment', EP_PAGES );
    add_rewrite_endpoint( 'equipmentSelectinterests', EP_PAGES );
}
add_action( 'init', 'desego_add_my_account_endpoints' );


//Agrega contenido al endpoint "equipmentDashboard" [woocommerce_account_{your-endpoint-slug}_endpoint]
function desego_equipmentDashboard_endpoint_content() {
    get_template_part('partials/equipment/dashboard');
}
add_action( 'woocommerce_account_equipmentDashboard_endpoint', 'desego_equipmentDashboard_endpoint_content' );

// Agrega contenido para seleccionar área
function desego_equipmentSelectarea_endpoint_content() {
    get_template_part('partials/equipment/selectarea');
}
add_action( 'woocommerce_account_equipmentSelectarea_endpoint', 'desego_equipmentSelectarea_endpoint_content' );

// Agrega contenido para seleccionar equipos
function desego_equipmentSelectequipment_endpoint_content() {
    get_template_part('partials/equipment/selectequipment');
}
add_action( 'woocommerce_account_equipmentSelectequipment_endpoint', 'desego_equipmentSelectequipment_endpoint_content' );

// Agrega contenido para seleccionar líneas de interés
function desego_equipmentSelectinterests_endpoint_content() {
    get_template_part('partials/equipment/selectinterests');
}
add_action( 'woocommerce_account_equipmentSelectinterests_endpoint', 'desego_equipmentSelectinterests_endpoint_content' );

//Acciones de formulario para SELECCIONAR ÁREA
//usuarios logueados
add_action( 'admin_post_desego_equipment_save_area', 'desego_equipment_save_area' );

function desego_equipment_save_area() {
    $redirect = wc_get_account_endpoint_url('equipmentSelectarea').'?&status=0';
    if (!empty($_POST)){
        $area_id = isset( $_POST['area'] ) ? $_POST['area'] : 0;

        //verifica que los valores introducidos sean enteros (IDs). Sirve como sanitize y validación ya que solo existen IDs enteros
        $valid = $area_id == 1 || $area_id == 2;
       
        if($valid){
            //obtener si existe registro actual
            global $wpdb;
            $user_id = get_current_user_id();
            $table_name = $wpdb->prefix . 'desego_equipment';
            $success = 0;
            $registry = $wpdb->get_row( "SELECT * FROM $table_name WHERE user_id = $user_id");
            $now = date("Y-m-d H:i:s");

            if(!$registry){
                $success = $wpdb->insert( 
                    $table_name, 
                    array(
                        'user_id' => $user_id, 
                        'area' => $area_id,
                        'created' => $now,
                        'updated' => $now
                    ), 
                    array( 
                        '%d',
                        '%d',
                        '%s',
                        '%s'
                    ) 
                );
            }else{
                $now = date("Y-m-d H:i:s");
                $success = $wpdb->update( 
                    $table_name, 
                    array( 
                        'area' => $area_id,
                        'updated' => $now,
                    ), 
                    array( 'id' => $registry->id ), 
                    array( 
                        '%d',
                        '%s'
                    ),
                    array( '%d' ) 
                );
            }

            if($success) $redirect =  wc_get_account_endpoint_url('equipmentSelectequipment');

        }
    }
    wp_redirect( $redirect );
    die;
}

 //Acciones de formulario para SELECCIONAR PRODUCTOS Y LINEAS DE INTERÉS    
 //usuarios logueados
 add_action( 'admin_post_desego_equipment_save_equipment_interests', 'desego_equipment_save_equipment_interests' );

 function desego_equipment_save_equipment_interests() {
    $redirect = wc_get_account_endpoint_url('equipmentDashboard').'?&status=0';
    
    if (!empty($_POST)){
        $type= isset( $_POST['type'] ) ? $_POST['type'] : '';
        $ids = isset( $_POST[$type] ) ? (array) $_POST[$type] : array();


        //verifica que los valores introducidos sean enteros (IDs). Sirve como sanitize y validación ya que solo existen IDs enteros. Si es vacío también lo guarda por si deseleccionaron todo
        $valid = ctype_digit(implode('',$ids)) || empty($ids);

        if($valid && $type){
            //obtener si existe registro actual
            global $wpdb;
            $user_id = get_current_user_id();
            $table_name = $wpdb->prefix . 'desego_equipment';
            $success = 0;
            $registry = $wpdb->get_row( "SELECT * FROM $table_name WHERE user_id = $user_id");
            $now = date("Y-m-d H:i:s");

            if(!$registry){
                $success = $wpdb->insert( 
                    $table_name, 
                    array(
                        'user_id' => $user_id, 
                        $type => empty($ids) ? '' : json_encode($ids, JSON_NUMERIC_CHECK),
                        'created' => $now, 
                        'updated' => $now, 
                    ), 
                    array( 
                        '%d',
                        '%s',
                        '%s',
                        '%s'
                    ) 
                );
            }else{
                $success = $wpdb->update( 
                    $table_name, 
                    array( 
                        $type => json_encode($ids, JSON_NUMERIC_CHECK),
                        'updated' => $now,
                    ), 
                    array( 'id' => $registry->id ), 
                    array( 
                        '%s',
                        '%s'
                    ),
                    array( '%d' ) 
                );
            }

            if($success && $type=='equipment'){
                $redirect = wc_get_account_endpoint_url('equipmentSelectinterests');
            }elseif($success && $type=='interests'){
                $redirect = wc_get_account_endpoint_url('equipmentDashboard');
            }

        }
    }
    wp_redirect( $redirect );
    die;
 }

//Action del formulario si es un usuario no logueado
add_action( 'admin_post_nopriv_desego_equipment_save_area', 'desego_no_logged' );
add_action( 'admin_post_nopriv_desego_equipment_save_equipment_interests', 'desego_no_logged' );
function desego_no_logged() {
    $redirect = wc_get_account_endpoint_url('equipmentDashboard');
}

//AJAX load equipment
function desego_load_equipment(){
    $area = (isset($_POST['area'])) ? $_POST['area'] : 0;
    $category = (isset($_POST['category'])) ? $_POST['category'] : 0;
    $paged = (isset($_POST['paged'])) ? $_POST['paged'] : 1;

    $tax_query = array(
        'relation' => 'AND',
        array(
            'taxonomy'        => 'pa_tipo',
            'field'           => 'slug',
            'terms'           =>  array('equipos'),
            'operator'        => 'IN',
        )
    );

    if($area){
        $tax_query[] = array(
            'taxonomy'        => 'pa_area',
            'field'           => 'slug',
            'terms'           =>  array($area),
            'operator'        => 'IN',
        );
    }

    if($category){
        $tax_query[] = array(
            'taxonomy'        => 'pa_categoria',
            'field'           => 'slug',
            'terms'           =>  array($category),
            'operator'        => 'IN',
        );
    }

    $equipment_args = array(
        'post_type'      => array('product'),
        'post_status'    => 'publish',
        'posts_per_page' => 12,
        'paged' => $paged,
        'meta_key' => 'equipment_module_order',
        'orderby' => 'meta_value',
        'order' => 'ASC',
        'meta_query'     => array(
            array(
                'key'     => 'in_equipment_module',
                'value'   => 'yes',
            )
        ),
        'tax_query'      => $tax_query
     );
    
    $equipment = new WP_Query( $equipment_args );

    $output = '';
    if($equipment -> have_posts()): 
        if($paged<=1) $output.= '<input type="hidden" id="max-pages" value="'.$equipment->max_num_pages.'" />';   
        woodmart_lazy_loading_init(true);
        while($equipment -> have_posts()): $equipment->the_post();
            $output .= get_template_part('partials/equipment/loop-equipment');
        endwhile;
        woodmart_lazy_loading_deinit(true);
        wp_reset_postdata();
    else:
        if($paged<=1) $output.= '<input type="hidden" id="max-pages" value="0" />';
        $output.="<p>No hay equipos</p>";
    endif;

    die($output); 
}

add_action('wp_ajax_desego_load_equipment', 'desego_load_equipment');
add_action('wp_ajax_nopriv_desego_load_equipment', 'desego_no_logged');

//Popup de nuevas funcionalidades
//Agregar link del popup a "mi cuenta"
function add_video_popup_link() {
    echo '<a class="video pill" id="popup-video" href="#"><i class="fa fa-video-camera"></i> Ver nuevas funcionalidades</a><br>';
}

function add_video_popup_script(){
    get_template_part('partials/equipment/videopopupscript');
}
//add_action( 'woocommerce_account_dashboard', 'add_video_popup_link', 9, 0 );
//add_action( 'wp_footer', 'add_video_popup_script', 10);

//funcion ajax para setear el video como visto
function desego_set_video_watched(){
    setcookie('videopopup_watched', 1, time()+31536000000, '/', $_SERVER['HTTP_HOST']);
}
// add_action('wp_ajax_desego_set_video_watched', 'desego_set_video_watched');
// add_action('wp_ajax_nopriv_desego_set_video_watched', 'desego_no_logged');


//Agregar campo "ver en mis equipos" a los productos de woocommerce
add_action( 'woocommerce_product_options_advanced', 'desego_equipment_option_group' );
function desego_equipment_option_group() {
	echo '<div class="options_group">';
 
	woocommerce_wp_checkbox( array(
		'id'      => 'in_equipment_module',
		'value'   => get_post_meta( get_the_ID(), 'in_equipment_module', true ),
		'label'   => 'Mostrar en "mis equipos"',
		'desc_tip' => true,
		'description' => 'Este producto aparecerá en la sección de "mis equipos", siempre y cuando su atributo <i>Tipo</i> sea <i>Equipos</i>',
	) );
	woocommerce_wp_text_input( array(
		'id'      => 'equipment_module_order',
		'value'   => get_post_meta( get_the_ID(), 'equipment_module_order', true ),
		'label'   => 'Orden',
		'desc_tip' => true,
		'description' => 'Se usa para determinar el orden en que aparecerá en la sección "mis equipos". Tiene que ser un valor numérico',
	) );
 
	echo '</div>';
}

add_action( 'woocommerce_process_product_meta', 'desego_wc_save_fields', 10, 2 );
function desego_wc_save_fields( $id, $post ){
    update_post_meta( $id, 'in_equipment_module', $_POST['in_equipment_module'] );
    update_post_meta( $id, 'equipment_module_order', $_POST['equipment_module_order'] );
}