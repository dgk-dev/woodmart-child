<?php
//Subir este archivo a wp-content/mu-plugins para que sea ejecutado antes de todos los plugins y temas. Esto sirve para sobreescribir algunas funciones de los plugins que no se pueden redeclarar en el child.


//Redondeo de puntos en plugin rewardsystem. No existe una manera de decirle al plugin que redondee a un solo decimal, solo tiene la opcion para hacerlo a 2 decimales. Esta funcion sobreescribe la principal del plugin apra hacerlo siempre a 1
function round_off_type( $points , $args = array() ) {
    return round( $points , 1 ) ;
}