<?php
//add id to shortocde params
add_filter('woodmart_get_products_shortcode_params', 'woodmart_products_custom_shortcode_params');

function woodmart_products_custom_shortcode_params($params){
    $params[]= array(
        'type' => 'textfield',
        'heading' => esc_html__( 'Desego custom data', 'woodmart' ),
        'param_name' => 'desego_custom_id',
        'hint' => 'Uso de desarrollador: agrega un ID para utilizar hooks personalizados en filtrado de información',
    );
    // var_dump($params);

    return $params;
}

// overwrite woodmart chorcode
function woodmart_shortcode_products( $atts, $query = false ) {
		
    $parsed_atts = shortcode_atts( woodmart_get_default_product_shortcode_atts(), $atts );
    
    extract( $parsed_atts );
    
    $is_ajax = ( defined( 'DOING_AJAX' ) && DOING_AJAX && $force_not_ajax != 'yes' );
    
    $parsed_atts['force_not_ajax'] = 'no'; // :)
    
    $encoded_atts = json_encode( $parsed_atts );
    
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    
    if ( $ajax_page > 1 ) $paged = $ajax_page;
    
    $ordering_args = WC()->query->get_catalog_ordering_args( $orderby, $order );
    
    $meta_query   = WC()->query->get_meta_query();
    
    $tax_query   = WC()->query->get_tax_query();
    
    if ( $post_type == 'new' ){
        $meta_query[]           = array(
            'relation' => 'OR',
            array(
                'key'     => '_woodmart_new_label',
                'value'   => 'on',
                'compare' => 'IN',
            ),
            array(
                'key'     => '_woodmart_new_label_date',
                'value'   => date( 'Y-m-d' ),
                'compare' => '>',
                'type'    => 'DATE',
            ),
        );
    }
    
    if ( $orderby == 'post__in' ) {
        $ordering_args['orderby'] = $orderby;
    }
    
    $args = array(
        'post_type' 			=> 'product',
        'post_status' 			=> 'publish',
        'ignore_sticky_posts' 	=> 1,
        'paged' 			  	=> $paged,
        'orderby'             	=> $ordering_args['orderby'],
        'order'               	=> $ordering_args['order'],
        'posts_per_page' 		=> $items_per_page,
        'meta_query' 			=> $meta_query,
        'tax_query'             => $tax_query,
    );
    
    if ( ! empty( $ordering_args['meta_key'] ) ) {
        $args['meta_key'] = $ordering_args['meta_key'];
    }
    
    if ( ! empty( $meta_key ) ) {
        $args['meta_key'] = $meta_key;
    }
    
    if ( $post_type == 'ids' && $include != '' ) {
        $args['post__in'] = array_map('trim', explode(',', $include) );
    }
    
    if ( ! empty( $exclude ) ) {
        $args['post__not_in'] = array_map('trim', explode(',', $exclude) );
    }
    
    if ( ! empty( $taxonomies ) ) {
        $taxonomy_names = get_object_taxonomies( 'product' );
        $terms = get_terms( $taxonomy_names, array(
            'orderby' => 'name',
            'include' => $taxonomies,
            'hide_empty' => false,
        ));
        
        if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {
            if ( $post_type == 'featured' ) $args['tax_query'] = array( 'relation' => 'AND' );
            
            $relation = $query_type ? $query_type : 'OR';
            if ( count( $terms ) > 1 ) $args['tax_query']['categories'] = array( 'relation' => $relation );
            
            foreach ( $terms as $term ) {
                $args['tax_query']['categories'][] = array(
                    'taxonomy' => $term->taxonomy,
                    'field' => 'slug',
                    'terms' => array( $term->slug ),
                    'include_children' => true,
                    'operator' => 'IN'
                );
            }
        }
    }
    
    if ( $post_type == 'featured' ) {
        $args['tax_query'][] = array(
            'taxonomy' => 'product_visibility',
            'field'    => 'name',
            'terms'    => 'featured',
            'operator' => 'IN',
            'include_children' => false,
        );
    }
    
    if ( apply_filters( 'woodmart_hide_out_of_stock_items', false ) && 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
        $args['meta_query'][] = array( 'key' => '_stock_status', 'value' => 'outofstock', 'compare' => 'NOT IN' );
    }
    
    if ( ! empty( $order ) ) {
        $args['order'] = $order;
    }
    
    if ( ! empty( $offset ) ) {
        $args['offset'] = $offset;
    }
    
    
    if ( $post_type == 'sale' ) {
        $args['post__in'] = array_merge( array( 0 ), wc_get_product_ids_on_sale() );
    }
    
    if ( $post_type == 'bestselling' ) {
        $args['orderby'] = 'meta_value_num';
        $args['meta_key'] = 'total_sales';
        $args['order'] = 'DESC';
    }

    $args = apply_filters('desego_custom_products_args', $args, $atts['desego_custom_id']);
    
    if ( empty( $product_hover ) || $product_hover == 'inherit' ) $product_hover = woodmart_get_opt( 'products_hover' );
    
    if ( $sale_countdown ) {
        woodmart_enqueue_script( 'woodmart-countdown' );
    }
    
    woodmart_set_loop_prop( 'timer', $sale_countdown );
    woodmart_set_loop_prop( 'progress_bar', $stock_progress_bar );
    woodmart_set_loop_prop( 'product_hover', $product_hover );
    woodmart_set_loop_prop( 'products_view', $layout );
    woodmart_set_loop_prop( 'is_shortcode', true );
    woodmart_set_loop_prop( 'img_size', $img_size );
    woodmart_set_loop_prop( 'products_columns', $columns );

    if ( $products_masonry ) woodmart_set_loop_prop( 'products_masonry', ( $products_masonry == 'enable' ) ? true : false );
    if ( $product_quantity ) woodmart_set_loop_prop( 'product_quantity', ( $product_quantity == 'enable' ) ? true : false );
    if ( $products_different_sizes ) woodmart_set_loop_prop( 'products_different_sizes', ( $products_different_sizes == 'enable' ) ? true : false );
    
    if ( 'top_rated_products' === $post_type ) {
        add_filter( 'posts_clauses', 'woodmart_order_by_rating_post_clauses' );
        $products = new WP_Query( $args );
        remove_filter( 'posts_clauses', 'woodmart_order_by_rating_post_clauses' );
    } else {
        $products = new WP_Query( $args );
    }
    
    WC()->query->remove_ordering_args();
    
    $parsed_atts['custom_sizes'] = apply_filters( 'woodmart_products_shortcode_custom_sizes', false );
    
    // Simple products carousel
    if ( $layout == 'carousel' ){
        return woodmart_generate_posts_slider( $parsed_atts, $products );
    }
    
    if ( $pagination != 'arrows' ) {
        woodmart_set_loop_prop( 'woocommerce_loop', $items_per_page * ( $paged - 1 ) );
    }
    
    if ( $layout == 'list' ) {
        $class .= ' elements-list';
    } else {
        if ( ! $highlighted_products ) {
            $class .= ' woodmart-spacing-' . $spacing;
        }
        $class .= ' grid-columns-' . $columns;
    }
    
    $class .= ' pagination-' . $pagination;
    
    if ( woodmart_loop_prop( 'products_masonry' ) ) {
        $class .= ' grid-masonry';
        woodmart_enqueue_script( 'isotope' );
        woodmart_enqueue_script( 'woodmart-packery-mode' );
    }
    
    $products_element_classes = $highlighted_products ? ' woodmart-highlighted-products' : '';
    $products_element_classes .= ( $element_title ) ? ' with-title' : '';
    $products_element_classes .= $el_class ? ' ' . $el_class : '';
    
    if ( $products_bordered_grid && ! $highlighted_products ) {
        $class .= ' products-bordered-grid';
    }
    
    if ( woodmart_loop_prop( 'product_quantity' ) ) {
        $class .= ' wd-quantity-enabled';
    }
    
    if ( 'none' !== woodmart_get_opt( 'product_title_lines_limit' ) && $layout !== 'list' ) {
        $class .= ' title-line-' . woodmart_get_opt( 'product_title_lines_limit' );
    }
    
    if ( $lazy_loading == 'yes' ) {
        woodmart_lazy_loading_init( true );
    }
    
    ob_start();
    
    if ( ! $is_ajax ) echo '<div class="woodmart-products-element' . esc_attr( $products_element_classes ) . '">';
    
    if ( ! $is_ajax && $pagination == 'arrows' ) echo '<div class="woodmart-products-loader"></div>';
    
    if ( ! $is_ajax ) echo '<div class="products elements-grid align-items-start row woodmart-products-holder ' . esc_attr( $class ) . '" data-paged="1" data-atts="' . esc_attr( $encoded_atts ) . '" data-source="shortcode">';
    
    //Element title
    if ( ( ! $is_ajax || $pagination == 'arrows' ) && $element_title ) echo '<h4 class="title element-title col-12">' . esc_html( $element_title ) . '</h4>';
    
    if ( $products->have_posts() ) :
        while ( $products->have_posts() ) :
            $products->the_post();
            wc_get_template_part( 'content', 'product' );
        endwhile;
    endif;
    
    if ( ! $is_ajax ) echo '</div>';
    
    if ( function_exists( 'woocommerce_reset_loop' ) ) woocommerce_reset_loop();
    
    wp_reset_postdata();
    
    woodmart_reset_loop();
    
    if ( $lazy_loading == 'yes' ) {
        woodmart_lazy_loading_deinit();
    }
    
    if ( $products->max_num_pages > 1 && ! $is_ajax && $pagination ) {
        ?>
        <div class="products-footer">
            <?php if ( $pagination == 'more-btn' || $pagination == 'infinit' ): ?>
                <a href="#" rel="nofollow" class="btn woodmart-load-more woodmart-products-load-more load-on-<?php echo 'more-btn' === $pagination ? 'click' : 'scroll'; ?>"><span class="load-more-label"><?php esc_html_e( 'Load more products', 'woodmart' ); ?></span></a>
                <div class="btn woodmart-load-more woodmart-load-more-loader"><span class="load-more-loading"><?php esc_html_e('Loading...', 'woodmart'); ?></span></div>
            <?php elseif ( $pagination == 'arrows' ): ?>
                <div class="wrap-loading-arrow">
                    <div class="woodmart-products-load-prev disabled"><?php esc_html_e( 'Load previous products', 'woodmart' ); ?></div>
                    <div class="woodmart-products-load-next"><?php esc_html_e( 'Load next products', 'woodmart' ); ?></div>
                </div>
            <?php elseif ( $pagination == 'links' ): ?>
                <nav class="woocommerce-pagination">
                    <?php
                    $url = woodmart_get_whishlist_page_url();
                    $id  = get_query_var( 'wishlist_id' );
                    
                    if ( $id && $id > 0 ) {
                        $url .= $id . '/';
                    }
                    
                    if ( '' != get_option( 'permalink_structure' ) ) {
                        $base = user_trailingslashit( $url . 'page/%#%' );
                    } else {
                        $base = add_query_arg( 'page', '%#%', $url );
                    }
                    
                    add_filter( 'get_pagenum_link', '__return_false' );
                    
                    echo paginate_links( //phpcs:ignore
                        array(
                            'base'      => $base,
                            'add_args'  => true,
                            'total'     => $products->max_num_pages,
                            'prev_text' => '&larr;',
                            'next_text' => '&rarr;',
                            'type'      => 'list',
                            'end_size'  => 3,
                            'mid_size'  => 3,
                        )
                    );
                    
                    remove_filter( 'get_pagenum_link', '__return_false' );
                    ?>
                </nav>
            <?php endif ?>
        </div>
        <?php
    }
    
    if ( ! $is_ajax ) echo '</div>';
    
    $output = ob_get_clean();
    
    if ( $is_ajax ) {
        $output =  array(
            'items' => $output,
            'status' => ( $products->max_num_pages > $paged ) ? 'have-posts' : 'no-more-posts'
        );
    }
    
    return $output;
    
}

add_filter('desego_custom_products_args', 'desego_product_suggestions', 10, 2);

function desego_product_suggestions($args, $desego_custom_id){
    if($desego_custom_id == 'suggestions'){

        $recently_viewed_products = isset($_COOKIE['woocommerce_recently_viewed']) ? explode('|', $_COOKIE['woocommerce_recently_viewed']) : array();
        if(empty($recently_viewed_products)) return $args;
        
       $categories_att = array();
       $categories_tax = array();
        foreach($recently_viewed_products as $product_id){
            $terms_att = get_the_terms($product_id, 'pa_categoria');
            $terms_tax = get_the_terms($product_id, 'product_cat');
            
            foreach($terms_att as $term_att){
                if(!in_array($term_att->term_id, $categories_att))
                    $categories_att[]= $term_att->term_id;
            }
            foreach($terms_tax as $term_tax){
                if(!in_array($term_tax->term_id, $categories_tax))
                    $categories_tax[]= $term_tax->term_id;
            }

        }

        if(empty($categories_tax) && empty($categories_att)) return $args;

        $args['meta_query'] = array(
            array(
                'key' => '_stock_status',
                'value' => 'instock'
            ),
            array(
                'key' => '_backorders',
                'value' => 'no'
            ),
        );
        $args['tax_query'] =  array(
            array(
                'relation' => 'OR',
                array(
                    'taxonomy' => 'pa_categoria',
                    'field' => 'term_id',
                    'terms' => $categories_att
                ),
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'term_id',
                    'terms' => $categories_tax
                )
            ),
            array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'slug',
                    'terms' => 'producto-por-puntos',
                    'operator' => 'NOT IN'
                )
            )
        );
           
    }

    return $args;
}