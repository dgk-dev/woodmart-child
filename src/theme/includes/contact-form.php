<?php

add_action('wp_ajax_nopriv_desego_contact_form','desego_contact_form');
add_action('wp_ajax_desego_contact_form','desego_contact_form');

function desego_contact_form(){
    $data = $_POST;
    $validation = desego_contact_form_validation($data);
    if(!empty($validation['errors'])){
        wp_send_json_error( $validation );
    }else{
        // Construir nueva data para envío
            // Quitar action de wordpress
        unset($data['action']);
            // Duplicar campo email con nuevo nombre
        $data['00N1a000003Rojt'] = $data['email'];
            // Duplicar campo teléfono con nuevo nombre
        $data['00N1a00000633d0'] = $data['phone'];

        //enviar resultado
        $result = desego_contact_form_send($data);
        ($result!==FALSE) ? wp_send_json_success() : wp_send_json_error();
    }
	
}

function desego_contact_form_validation($data){
    $data['errors'] = array();
    $required_message = 'Por favor, complete el campo requerido.';
    $valid_email_message = 'La dirección e-mail parece inválida.';
    $valid_number_message = 'El número de teléfono parece inválido.';

    // Validar asunto: required
    if (strlen($_POST['00N1a000003RnzJ']) <= 0) {
        $data['errors']['00N1a000003RnzJ'] = $required_message;
    }

    // Validar nombre: required
    if (strlen($_POST['first_name']) <= 0) {
        $data['errors']['first_name'] = $required_message;
    }

    // Validar apellidos: required
    if (strlen($_POST['last_name']) <= 0) {
        $data['errors']['last_name'] = $required_message;
    }

    // Validar email: required, valid email
    if (strlen($_POST['email']) <= 0) {
        $data['errors']['email'] = $required_message;
    }else if(!is_email($_POST['email'])){
        $data['errors']['email'] = $valid_email_message;
    }

    // Validar teléfono: required, number
    if (strlen($_POST['phone']) <= 0) {
        $data['errors']['phone'] = $required_message;
    }else if(!is_numeric($_POST['phone'])){
        $data['errors']['phone'] = $valid_number_message;
    }

    // Validar celular: number
    if (strlen($_POST['mobile']) > 0 && !is_numeric($_POST['mobile'])) {
        $data['errors']['mobile'] = $valid_number_message;
    }

    // Validar país: required
    if (strlen($_POST['country_code']) <= 0) {
        $data['errors']['country_code'] = $required_message;
    }

    // Validar estado/provincia: required
    if (strlen($_POST['state_code']) <= 0) {
        $data['errors']['state_code'] = $required_message;
    }

    // Validar estado/provincia: required
    if (strlen($_POST['sector']) <= 0) {
        $data['errors']['sector'] = $required_message;
    }

    return $data;
}

function desego_contact_form_send($data){
    $url = 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';

    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    return $result;
}
