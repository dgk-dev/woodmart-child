<?php

add_action('wp_ajax_nopriv_desego_support_form','desego_support_form');
add_action('wp_ajax_desego_support_form','desego_support_form');

function desego_support_form(){
    $data = $_POST;
    $validation = desego_support_form_validation($data);
    if(!empty($validation['errors'])){
        wp_send_json_error( $validation );
    }else{
        // Construir nueva data para envío
            // Quitar action de wordpress
        unset($data['action']);

        //enviar resultado
        $result = desego_support_form_send($data);
        ($result!==FALSE) ? wp_send_json_success($data) : wp_send_json_error();
    }
	
}

function desego_support_form_validation($data){
    $data['errors'] = array();
    $required_message = 'Por favor, complete el campo requerido.';
    $valid_email_message = 'La dirección e-mail parece inválida.';
    $valid_number_message = 'El número de teléfono tiene que tener 10 dígitos.';

    // Validar motivo del caso: required
    if (strlen($_POST['reason']) <= 0) {
        $data['errors']['reason'] = $required_message;
    }

    // Validar nombre: required
    if (strlen($_POST['name']) <= 0) {
        $data['errors']['name'] = $required_message;
    }

    // Validar teléfono: required, number
    if (strlen($_POST['phone']) <= 0) {
        $data['errors']['phone'] = $required_message;
    }else if(!is_numeric($_POST['phone']) || strlen($_POST['phone']) != 10){
        $data['errors']['phone'] = $valid_number_message;
    }
   

    // Validar email: required, valid email
    if (strlen($_POST['email']) <= 0) {
        $data['errors']['email'] = $required_message;
    }else if(!is_email($_POST['email'])){
        $data['errors']['email'] = $valid_email_message;
    }

    // Validar equipo: required
    if (strlen($_POST['00N1a000008zTNe']) <= 0) {
        $data['errors']['00N1a000008zTNe'] = $required_message;
    }
    
    // Validar descripción: required
    if (strlen($_POST['description']) <= 0) {
        $data['errors']['description'] = $required_message;
    }

    return $data;
}

function desego_support_form_send($data){
    $url = 'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8';

    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    return $result;
}
