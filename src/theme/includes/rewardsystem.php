<?php
//Copia el archivo mu-desego.php a la carpeta correspondiente al activar el tema, este archivo contiene el redondeo a 1 del sistema de puntos. Esta parte ya hay que realizarla manual dado que algunos servidores no aceptan la instrucción directa de copy_dir por razones de seguridad
// add_action('after_switch_theme', 'desego_copy_mu_plugins');
// function desego_copy_mu_plugins(){
//     $mu_dir = get_stylesheet_directory() . '/includes/mu-plugins/';
//     $dest_dir = WP_CONTENT_DIR.'/mu-plugins/';
//     if (!file_exists($dest_dir)) mkdir($dest_dir, 0755, true);

//     copy_dir( 
//         $mu_dir, 
//         $dest_dir
//     );
// }

//Quita un script inline innecesario de jquery en el head.
remove_action( 'wp_head' , array( 'RSPointPriceFrontend', 'redirect_if_coupon_removed' ) ) ;

//shortcode para mostrar puntos con link a mis puntos
add_shortcode('desego_rewards_header', 'desego_rewards_points_header'); 
function desego_rewards_points_header() { 
    if(!is_user_logged_in() || !class_exists('RS_Points_Data')) return '';
    $PointsData = new RS_Points_Data(get_current_user_id()) ;
    $Points = $PointsData->total_available_points() ;
    $url_title = get_option( 'rs_my_reward_url_title' ) != '' ? get_option( 'rs_my_reward_url_title' ) : 'sumo-rewardpoints' ;
    $url = wc_get_account_endpoint_url($url_title);
    $output = '<div style="margin-bottom: 3px;">Mis puntos: <a href="'. $url . '">' . round_off_type( number_format( ( float ) $Points , 2 , '.' , '' ) ) . '</a></div>' ;
    return $output;
} 