<?php

/**
 * ------------------------------------------------------------------------------------------------
 * Agregar botón comprar ahora
 * ------------------------------------------------------------------------------------------------
 */
//add_action('woocommerce_after_add_to_cart_button','custom_content_after_addtocart_button',100);
function custom_content_after_addtocart_button()
{
	global $product;
	$id = $product->get_id();
	$link = do_shortcode('[add_to_cart_url id="' . absint($id) . '"]');
	echo '<input type="hidden" id="is_buy_now" name="is_buy_now" value="0"><button type="submit" id="buy_now_button" class="button alt" href="' . esc_url($link) . '">' . __('COMPRAR AHORA', 'textdomain') . '</button>';
}

add_filter('woocommerce_add_to_cart_redirect', 'redirect_to_checkout');
function redirect_to_checkout($redirect_url)
{
	if (isset($_REQUEST['is_buy_now']) && $_REQUEST['is_buy_now']) {
		global $woocommerce;
		$redirect_url = wc_get_checkout_url();
	}
	return $redirect_url;
}

// Cambiar texto de botones de compra
add_filter('woocommerce_product_add_to_cart_text', 'bbloomer_custom_add_cart_button_loop', 99, 2);
function bbloomer_custom_add_cart_button_loop($label, $product)
{

	if ($product->get_type() == 'simple' && $product->is_purchasable() && $product->is_in_stock()) {
		$label = __('Añadir', 'woocommerce');

		foreach (WC()->cart->get_cart() as $cart_item_key => $values) {
			$_product = $values['data'];
			if (get_the_ID() == $_product->get_id()) {
				$label = __('Añadir', 'woocommerce');
			}
		}
	}

	return $label;
}

//* Oculta resto de métodos de envío cuando envío gratuito está disponible
add_filter('woocommerce_package_rates', 'desego_hide_shipping_when_free_is_available', 100);
function desego_hide_shipping_when_free_is_available($rates)
{
	$free = array();
	foreach ($rates as $rate_id => $rate) {
		if ('free_shipping' === $rate->method_id) {
			$free[$rate_id] = $rate;
			break;
		}
	}
	return !empty($free) ? $free : $rates;
}

//Login ruta
add_filter('login_url', 'login_linkchanger');
function login_linkchanger($link)
{
	/*whatever you need to do with the link*/
	return home_url('/mi-cuenta');
}

// No mostrar los productos de la categoría reactivos en la página principal de la tienda
add_action('woocommerce_product_query', 'prefix_custom_pre_get_posts_query');
function prefix_custom_pre_get_posts_query($q)
{

	if (is_page('promociones')) { // set conditions here
		$tax_query = (array) $q->get('tax_query');

		$tax_query[] = array(
			'taxonomy' => 'pa_tipo',
			'field'    => 'slug',
			'terms'    => array('reactivos'), // set product categories here
			'operator' => 'NOT IN'
		);


		$q->set('tax_query', $tax_query);
	}
}

// Mostrar pdf Manual en descripción
add_filter('the_content', 'display_disclaimer_after_product_description', 10, 1);
function display_disclaimer_after_product_description($content)
{
	// Only for single product pages
	if (!is_product()) return $content;

	if (($product_manual = get_field('manual', get_the_id())) && ($product_ficha = get_field('ficha_tecnica', get_the_id()))) {
		return $content . '<a target="_blank" class="button" href="' . $product_manual . '"><i class="fa fa-download" aria-hidden="true"></i> Manual</a> <a target="_blank" class="button" href="' . $product_ficha . '"><i class="fa fa-download" aria-hidden="true"></i> Ficha técnica</a>';
	}

	return $content;
}


//Clonar menú categorías para mostrar en sticky header
add_filter('woodmart_header_clone_template', 'woodmart_sticky_header_categories');
function woodmart_sticky_header_categories()
{
	$template = '
        <div class="whb-sticky-header whb-clone whb-main-header">
            <div class="<%cloneClass%>">
                <div class="container">
                    <div class="whb-flex-row whb-general-header-inner">
                        <div class="whb-column whb-col-left whb-visible-lg">
                            <%.header-categories-nav%>
                        </div>
                        <div class="whb-column whb-col-center whb-visible-lg">
                            <%.main-nav%>
                        </div>
                        <div class="whb-column whb-col-right whb-visible-lg">
                            <%.woodmart-header-links%>
                            <%.search-button%>
                            <%.woodmart-wishlist-info-widget%>
                            <%.woodmart-shopping-cart%>
                        </div>
                        <%.whb-mobile-left%>
                        <%.whb-mobile-center%>
                        <%.whb-mobile-right%>
                    </div>
                </div>
            </div>
        </div>
    ';
	return $template;
}

add_filter('woocommerce_ship_to_different_address_checked', '__return_false');

// Plugin Name: WooCommerce Repeat Order Button.
// Disable/erase plugin. Code goes here
add_filter('woocommerce_my_account_my_orders_actions', 'desego_add_order_again_action', 10, 2);
function desego_add_order_again_action($actions, $order){
	if (!$order || !$order->has_status(apply_filters('woocommerce_valid_order_statuses_for_order_again', array('completed'))) || !is_user_logged_in()) {
		return $actions;
	}
	$actions['order-again'] = array(
		'url'  => wp_nonce_url(add_query_arg('order_again', $order->get_id()), 'woocommerce-order_again'),
		'name' => __('Volver a comprar', 'woocommerce')
	);

	return $actions;
}


//cart empty fix
remove_action('woocommerce_cart_is_empty', 'wc_empty_cart_message', 10);
add_action('woocommerce_cart_is_empty', 'desego_empty_cart_message', 10);
function desego_empty_cart_message(){
	echo '<p class="cart-empty">' . wp_kses_post(apply_filters('wc_empty_cart_message', __('Your cart is currently empty.', 'woocommerce'))) . '</p>';
}

//Rename menu elements. "Woocommerce" to "Tienda"
add_action( 'admin_menu', 'desego_wc_admin_rename_labels' );
function desego_wc_admin_rename_labels() {
    global $menu;
   
    $searchPlugin = "woocommerce";
    $replaceName = "Tienda";
    $replaceIcon = "dashicons-products";

    $menuItem = "";
    foreach($menu as $key => $item){
        if ( $item[2] === $searchPlugin ){
            $menuItem = $key;
        }
    }
    if($menuItem){
        $menu[$menuItem][0] = $replaceName;
        $menu[$menuItem][6] = $replaceIcon;
    }
}
//Replace woocommerce icon for dashicon "dashicons-products"
add_action('admin_head', 'desego_wc_admin_custom_icon');
function desego_wc_admin_custom_icon() {
  echo '<style>
    #adminmenu #toplevel_page_woocommerce .menu-icon-generic div.wp-menu-image::before {
        font-family: dashicons !important;
        content: "\f312" !important;
    }
  </style>';
}

//Ordenar por SKU. Función general.
add_filter('woocommerce_get_catalog_ordering_args', 'desego_woocommerce_get_catalog_ordering_args'); 
function desego_woocommerce_get_catalog_ordering_args( $args ) {
    $orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );

    switch($orderby_value){
        case 'sku_asc':
            $args['orderby'] = 'meta_value';
            $args['order'] = 'asc';
            $args['meta_key'] = '_sku';
        break;

        case 'sku_desc':
            $args['orderby'] = 'meta_value';
            $args['order'] = 'desc';
            $args['meta_key'] = '_sku';
        break;
    }

	return $args;
}

add_filter('woocommerce_catalog_orderby', 'desego_woocommerce_catalog_orderby');
add_filter( 'woocommerce_default_catalog_orderby_options', 'desego_woocommerce_catalog_orderby' );
function desego_woocommerce_catalog_orderby( $sortby ) {
	$sortby['sku_asc'] = 'Ordenar por SKU: A🠖Z';
	$sortby['sku_desc'] = 'Ordenar por SKU: Z🠖A';
	return $sortby;
}

//cambiar número de articulos por página de la sección reactivos e incluir atributo como categoría
add_action( 'woocommerce_product_query', 'reactives_product_query' );
function reactives_product_query( $q ) {
    if ( $q->is_main_query() && ( $q->get( 'wc_query' ) === 'product_query' ) && is_product_category( 'reactivos' ) ) {
		$tax_query = (array) $q->get('tax_query');

		$tax_query[] = array(
			'relation' => 'OR',
			array(
				'taxonomy' => 'pa_tipo',
				'field'    => 'slug',
				'terms'    => array('reactivos'),
				'operator' => 'IN'
			),
			array(
				'taxonomy' => 'product_cat',
				'field'    => 'slug',
				'terms'    => array('reactivos'),
				'operator' => 'IN'
			)
		);
		
		$q->set('tax_query', $tax_query);
        $q->set( 'posts_per_page', '50' );
    }
}

// Ocultar productos por puntos de toda la tienda de woocommerce
add_action( 'woocommerce_product_query', 'desego_remove_product_by_points' ); 
function desego_remove_product_by_points( $q ) {
    $tax_query = (array) $q->get( 'tax_query' );

    $tax_query[] = array(
        'taxonomy' => 'product_cat',
        'field' => 'slug',
        'terms' => array( 'producto-por-puntos' ),
        'operator' => 'NOT IN'
    );

    $q->set( 'tax_query', $tax_query );

}

// Extender búsqueda
add_filter( 'posts_search', 'desego_search_custom_query', 500, 2 );

function desego_search_custom_query( $search, $wp_query ){
	global $wpdb;
        
	if ( empty( $search ) || !empty($wp_query->query_vars['suppress_filters']) ) {
		return $search; // skip processing - If no search term in query or suppress_filters is true
	}

	$q = $wp_query->query_vars;
	$n = !empty($q['exact']) ? '' : '%';
	$search = $searchand = '';

	foreach ((array)$q['search_terms'] as $term ) {        
		$term = $n . $wpdb->esc_like( $term ) . $n;

		/* change query as per plugin settings */
		$OR = '';
		
		$search .= "{$searchand} (";

		// Name
		$search .= $wpdb->prepare("($wpdb->posts.post_title LIKE '%s')", $term);
		$OR = ' OR ';
		
		// Content
		$search .= $OR;
		$search .= $wpdb->prepare("($wpdb->posts.post_content LIKE '%s')", $term);
		$OR = ' OR ';
		
		// Excerpt
		$search .= $OR;
		$search .= $wpdb->prepare("($wpdb->posts.post_excerpt LIKE '%s')", $term);
		$OR = ' OR ';

		// Meta_keys
		// $meta_key_OR = '';
		// $meta_keys = array();
		// foreach ($meta_keys as $key_slug) {
		// 	$search .= $OR;
		// 	$search .= $wpdb->prepare("$meta_key_OR (espm.meta_key = '%s' AND espm.meta_value LIKE '%s')", $key_slug, $term);
		// 	$OR = '';
		// 	$meta_key_OR = ' OR ';
		// }
		// $OR = ' OR ';
	
		// Taxonomies
		$tax_OR = '';
		$taxonomies = array('product_cat', 'pa_categoria', 'pa_area', 'pa_marca');
		foreach ($taxonomies as $tax) {
			$search .= $OR;
			$search .= $wpdb->prepare("$tax_OR (estt.taxonomy = '%s' AND est.name LIKE '%s')", $tax, $term);
			$OR = '';
			$tax_OR = ' OR ';
		}

		$search .= ")";
		$searchand = " AND ";
	}

	if ( ! empty( $search ) ) {
		$search = " AND ({$search}) ";
		if ( ! is_user_logged_in() )
			$search .= " AND ($wpdb->posts.post_password = '') ";
	}

	 /* Join Table */
	 add_filter('posts_join_request', 'desego_search_join_table');

	 /* Request distinct results */
	 add_filter('posts_distinct_request', 'desego_search_distinct');

	return apply_filters('desego_posts_search', $search, $wp_query);
	
}

function desego_search_join_table($join){
	global $wpdb;
	// Meta keys
	// $join .= " LEFT JOIN $wpdb->postmeta espm ON ($wpdb->posts.ID = espm.post_id) ";

	// Taxonomies
	$join .= " LEFT JOIN $wpdb->term_relationships estr ON ($wpdb->posts.ID = estr.object_id) ";
	$join .= " LEFT JOIN $wpdb->term_taxonomy estt ON (estr.term_taxonomy_id = estt.term_taxonomy_id) ";
	$join .= " LEFT JOIN $wpdb->terms est ON (estt.term_id = est.term_id) ";
	return $join;
}

function desego_search_distinct($distinct) {
	$distinct = 'DISTINCT';
	return $distinct;
}

//Orden del menú mi cuenta en woocommerce
function desego_my_account_menu_order() {
    $menuOrder = array(
        'dashboard'          => __( 'Inicio', 'woocommerce' ),
        'equipmentDashboard' => __( 'Mis equipos', 'woocommerce' ),
        'orders'             => __( 'Pedidos', 'woocommerce' ),
        'downloads'          => __( 'Descargas', 'woocommerce' ),
        'edit-address'       => __( 'Direcciones', 'woocommerce' ),
        'edit-account'    => __( 'Detalles de la cuenta', 'woocommerce' ),
        'wishlist'    => __( 'Favoritos', 'woocommerce' ),
        'customer-logout'    => __( 'Salir', 'woocommerce' ),
    
    );

    if(class_exists( 'RS_Points_Data' )){
        $rewards_url_title = get_option( 'rs_my_reward_url_title' ) != '' ? get_option( 'rs_my_reward_url_title' ) : 'sumo-rewardpoints' ;
        $rewards_menu_item = array(
            $rewards_url_title => __( 'Mis puntos', 'woocommerce' )
        );
        $menuOrder= array_slice($menuOrder, 0, 2) + $rewards_menu_item + array_slice($menuOrder, 2);
    }

    return $menuOrder;
}
add_filter ( 'woocommerce_account_menu_items', 'desego_my_account_menu_order', 15 );

// Poner etiqueta "precio sin iva" o botón de contacto cuando el precio no esta disponible
add_filter( 'woocommerce_get_price_html', 'desego_get_price_html', 10, 2 ); 
function desego_get_price_html( $price, $instance ) {
	global $wp_query;
	if(is_product() && $wp_query->post->ID == $instance->get_ID()){
		if($price){
			$price = $price . ' <span class="no-tax muted">(Precio sin IVA)</span>';
		}else{
			$price = '<p class="price">Para conocer el precio del equipo, comunícate con nosotros.</p>
			<form name="cotizacion" action="/contacto/" method="POST">
				<input type="hidden" value="'.$instance->get_title().'" name="producto">
				<input type="submit" class="button" value="Contáctanos">
			</form>';
		}
	}
	return $price; 
}

// New order notification only for "Pending" Order status
add_action( 'woocommerce_new_order', 'pending_new_order_notification', 20, 1 );
function pending_new_order_notification( $order_id ) {

    // Get an instance of the WC_Order object
    $order = wc_get_order( $order_id );

    // Only for "pending" order status
    if( ! $order->has_status( 'pending' ) ) return;

    // Send "New Email" notification (to admin)
    WC()->mailer()->get_emails()['WC_Email_New_Order']->trigger( $order_id );
}


add_filter( 'woocommerce_cart_crosssell_ids', 'desego_crosssells_cart', 10, 2 );
function desego_crosssells_cart( $wp_parse_id_list, $cart ){
	$args = array(
		'post_type' => 'product',
		'posts_per_page' => -1,
		'fields' => 'ids',
	);
	foreach( $cart->get_cart() as $cart_item ){
		$args['meta_query'] = array(
			array(
				'key' => '_crosssell_ids',
				'value' => $cart_item['product_id'],
				'compare' => 'LIKE'
			)
		);
		$wp_parse_id_list = array_merge($wp_parse_id_list, get_posts($args));
	}
	return array_unique($wp_parse_id_list);
}

/**
 * Track product views. Always.
 */
function wc_track_product_view_always() {
    if ( ! is_singular( 'product' ) /* xnagyg: remove this condition to run: || ! is_active_widget( false, false, 'woocommerce_recently_viewed_products', true )*/ ) {
        return;
    }

    global $post;

    if ( empty( $_COOKIE['woocommerce_recently_viewed'] ) ) { // @codingStandardsIgnoreLine.
        $viewed_products = array();
    } else {
        $viewed_products = wp_parse_id_list( (array) explode( '|', wp_unslash( $_COOKIE['woocommerce_recently_viewed'] ) ) ); // @codingStandardsIgnoreLine.
    }

    // Unset if already in viewed products list.
    $keys = array_flip( $viewed_products );

    if ( isset( $keys[ $post->ID ] ) ) {
        unset( $viewed_products[ $keys[ $post->ID ] ] );
    }

    $viewed_products[] = $post->ID;

    if ( count( $viewed_products ) > 15 ) {
        array_shift( $viewed_products );
    }

    // Store for session only.
	wc_setcookie( 'woocommerce_recently_viewed', implode( '|', $viewed_products ) );
	
}

remove_action('template_redirect', 'wc_track_product_view', 20);
add_action( 'template_redirect', 'wc_track_product_view_always', 20 );

add_filter('woocommerce_product_cross_sells_products_heading', 'desego_cross_sells_heading', 10);
function desego_cross_sells_heading($heading){
	return 'Completa tu pedido';
}

// Desactivar envío gratis para productos usando ID
add_filter( 'woocommerce_shipping_free_shipping_is_available', 'desego_disable_free_shipping', 20 );  
function desego_disable_free_shipping( $is_available ) {
	global $woocommerce;

	// set the product ids that are ineligible
	$ineligible = array(
		'28583', // Kit de Prueba Rápida COVID-19 Ag Antígeno
		'16915' // Kit Prueba Rápida COVID-19 Ab (IgG/IgM)
	);

	// get cart contents
	$cart_items = $woocommerce->cart->get_cart();
	
	// loop through the items looking for one in the ineligible array
	foreach ( $cart_items as $item ) {
		if( in_array( $item['product_id'], $ineligible ) ) {
			return false;
		}
	}

	// nothing found return the default value
	return $is_available;
}

// Desactivar notificación de envío gratis para productos usando ID
add_filter( 'woocommerce_before_add_to_cart_button', 'desego_disable_free_shipping_notice', 20 );  
function desego_disable_free_shipping_notice( ) {
	global $product;
	$ineligible = array(
		'28583', // Kit de Prueba Rápida COVID-19 Ag Antígeno
		'16915' // Kit Prueba Rápida COVID-19 Ab (IgG/IgM)
	);
	if( in_array( $product->get_id(), $ineligible ) ) {
		echo '<style> .woodmart-after-add-to-cart .shipping__information li:nth-child(1){display:none} </style>';
	}
}