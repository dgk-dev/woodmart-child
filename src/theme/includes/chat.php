<?php
//Whatsapp
add_action('wp_footer','desego_add_footer_whatsapp');
function desego_add_footer_whatsapp(){
	$tel = "5214433734564 ";

	$url = "https://wa.me/${tel}";
	$img = get_stylesheet_directory_uri().'/img/whatsapp-icon.svg';
	echo "<div id='float-whatsapp' style='position:fixed;bottom:30px;left:30px;z-index:100; text-align:left'>";
	echo " <a href=${url} target='_blank'>";
	echo "<img src='${img}' width=40 height=40 />";
	echo " </a>";
	echo "</div>";
}