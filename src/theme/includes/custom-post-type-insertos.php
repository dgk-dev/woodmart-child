<?php
// Sección de insertos

function insertos_themes_taxonomy() {  
    register_taxonomy(  
        'insertos_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
        'insertos',        //post type name
        array(  
            'hierarchical' => true,  
            'label' => 'Categoría de insertos',  //Display name
            'query_var' => true,
            'show_admin_column' => true,
            'rewrite' => array(
                'slug' => 'inserto_category', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before 
            )
        )  
    );

    //update taxonomy name in database
    // UPDATE `wp_term_taxonomy` SET `taxonomy` = 'insertos_categories' WHERE `taxonomy` = 'themes_categories' 
}  
add_action( 'init', 'insertos_themes_taxonomy');

function insertos_filter_post_type_link($link, $post){
    if ($post->post_type != 'insertos')
        return $link;

    if ($cats = get_the_terms($post->ID, 'insertos_categories'))
        $link = str_replace('%insertos_categories%', array_pop($cats)->slug, $link);
    return $link;
}
add_filter('post_type_link', 'insertos_filter_post_type_link', 10, 2);
// Register Custom Faqs Type
function inserto_post_type() {

    $labels = array(
        'name'                  => _x( 'Insertos', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Inserto', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Insertos', 'text_domain' ),
        'name_admin_bar'        => __( 'Inserto', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Product:', 'text_domain' ),
        'all_items'             => __( 'Todos los insertos', 'text_domain' ),
        'add_new_item'          => __( 'Agregar nueva inserto', 'text_domain' ),
        'add_new'               => __( 'Nuevo inserto', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Editar inserto', 'text_domain' ),
        'update_item'           => __( 'Actualizar inserto', 'text_domain' ),
        'view_item'             => __( 'Ver inserto', 'text_domain' ),
        'search_items'          => __( 'Buscar inserto', 'text_domain' ),
        'not_found'             => __( 'No insertos disponibles', 'text_domain' ),
        'not_found_in_trash'    => __( 'No products found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Imagen principal', 'text_domain' ),
        'set_featured_image'    => __( 'Establecer imagen', 'text_domain' ),
        'remove_featured_image' => __( 'Eliminar imagen', 'text_domain' ),
        'use_featured_image'    => __( 'Utilizar como imagen principal', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Insertos', 'text_domain' ),
        'description'           => __( 'Insertos information pages.', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor','featured-image', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-slides',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,        
        'exclude_from_search'   => true,
        'publicly_queryable'    => false,
        'capability_type'       => 'page',
        'taxonomies'            => array( 'insertos_categories'),
    );
    register_post_type( 'insertos', $args );

}
add_action( 'init', 'inserto_post_type', 0 );

add_action('wp_enqueue_scripts', 'desego_insertos_filter_resources');
function desego_insertos_filter_resources()
{
	if (is_page('insertos')) {
		global $wp_query;
		
		wp_localize_script('desego-functions', 'insertosGlobalObj', array(
			'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', 
			'posts' => json_encode($wp_query->query_vars),
			'current_page' => $wp_query->query_vars['paged'] ? $wp_query->query_vars['paged'] : 1,
			'max_page' => $wp_query->max_num_pages
		));
	}

}

add_action('wp_ajax_insertos_filter', 'desego_insertos_filter');
add_action('wp_ajax_nopriv_insertos_filter', 'desego_insertos_filter');
function desego_insertos_filter(){

	$all     = false;
	$terms   = $_POST['params']['terms'];
	$page    = intval($_POST['params']['page']);
	$qty     = intval($_POST['params']['qty']);
	$tax_qry = [];

	global $wp_query;

	if (!is_array($terms)){
		$response = [
			'status'  => 501,
			'message' => 'La categoría no existe',
			'content' => 0
		];
		die(json_encode($response));
	}else{
		foreach ($terms as $tax => $slugs){
			if (in_array('all-terms', $slugs)) {
				$all = true;
			}
			$tax_qry[] = [
				'taxonomy' => $tax,
				'field'    => 'slug',
				'terms'    => $slugs,
			];
		}
	}

	$args = [
		'paged'          => $page,
		'post_type'      => 'insertos',
		'post_status'    => 'public',
		'posts_per_page' => $qty,
		'orderby' => 'title', // example: date
		'order'	=> 'asc', // example: ASC
	];


	if ($tax_qry && !$all) $args['tax_query'] = $tax_qry;

	$insertos = new WP_Query( $args );

	$posts_html = '';
	
	if ($insertos->have_posts()){
		$posts_html .= "<ul class='business-listings'>";
			if ($all) {
				$posts_html .= "<h2 class='business-category-title'>Todos los insertos</h2>";
			} else {
				$keys = array_keys($terms);
				$tax = $keys[0];
				$slug = $terms[$tax][0];
				$term = get_term_by('slug', $slug, $tax);
				$posts_html .= "<h2 class='business-category-title'>".$term->name."</h2>";
			}

			while ($insertos->have_posts()) : $insertos->the_post();
				$value = get_field("pdf");
				$valuesku = get_field("sku");
				// output insertos listing
				$posts_html .= "<li class='business-listing'><a href='" . $value . "' target='_blank'><i class='fa fa-download' aria-hidden='true'></i></a><span class='business-listing-sku'>" . $valuesku . "</span> <a href='" . $value . "' target='_blank'><h3 class='product-title'>" . get_the_title() . "</h3></a></li>";
			endwhile;
			$posts_html .= "</ul>";
		
	}else{
		$posts_html = '<p>No se encontraron resultados.</p>';
	}

	wp_die(json_encode(array(
		'posts' => json_encode($wp_query->query_vars),
		'max_page' => $wp_query->max_num_pages,
		'found_posts' => $wp_query->found_posts,
		'content' => $posts_html
	)));
}

//add_action('init', 'change_tax_name');
// function change_tax_name(){
//     global $wpdb;
//     $sql = "UPDATE `wp_term_taxonomy` SET `taxonomy` = 'insertos_categories' WHERE `taxonomy` = 'themes_categories'";
//     $results = $wpdb->get_results($sql);
// }