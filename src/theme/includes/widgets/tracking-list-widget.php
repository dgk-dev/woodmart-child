<?php

class Desego_Tracking_List_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
         
        // Base ID of your widget
        'desego_tracking_list_widget', 
         
        // Widget name will appear in UI
        __('Lista de paqueterías DESEGO', 'woodmart'), 
         
        // Widget description
        array( 'description' => __( 'Widget de lista de paqueterías para DESEGO', 'woodmart' ), ) );
    }
        
        
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        echo $args['before_widget'];

        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];
        ?>
     
        <ul class="desego-icon-list">
            <li>
                <a href="https://www.estafeta.com/" target="_BLANK">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/estafeta_footer_logo.png" alt="estafeta logo">
                </a>
            </li>
            <li>
                <a href="http://www.redpack.com.mx/" target="_BLANK">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/redpack_footer_logo.png" alt="redpack logo">
                </a>
            </li>
            <li>
                <a href="https://www.fedex.com/es-mx/home.html" target="_BLANK">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/fedex_footer_logo.png" alt="fedex logo">
                </a>
            </li>
            <li>
                <a href="https://www.dhl.com" target="_BLANK">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/dhl_footer_logo.png" alt="dhl logo">
                </a>
            </li>
        </ul>
        <?php
        echo $args['after_widget'];
    }

    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }else {
            $title = __( 'RASTREA TU PEDIDO', 'woodmart' );
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php 
        }
        // Updating widget replacing old instances with new
        public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}
// Register the widget
function register_desego_tracking_list_widget() {
    register_widget( 'Desego_Tracking_List_Widget' );
}
add_action( 'widgets_init', 'register_desego_tracking_list_widget' );