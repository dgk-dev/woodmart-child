<?php

class Desego_App_List_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
         
        // Base ID of your widget
        'desego_app_list_widget', 
         
        // Widget name will appear in UI
        __('Lista de tiendas para bajar la APP DESEGO', 'woodmart'), 
         
        // Widget description
        array( 'description' => __( 'Widget de lista de tiendas para bajar la APP de DESEGO', 'woodmart' ), ) );
    }
        
        
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        echo $args['before_widget'];

        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];
        ?>
     
        <ul class="desego-icon-list">
            <li>
                <a href="https://apps.apple.com/mx/app/desego/id1495116840" target="_BLANK">
                    <img class="w" src="<?php echo get_stylesheet_directory_uri(); ?>/img/appstore.png" alt="apptore logo">
                </a>
            </li>
            <li>
                <a href="https://play.google.com/store/apps/details?id=com.desego" target="_BLANK">
                    <img class="w" src="<?php echo get_stylesheet_directory_uri(); ?>/img/playstore.png" alt="playstore logo">
                </a>
            </li>
        </ul>
        <?php
        echo $args['after_widget'];
    }

    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }else {
            $title = __( 'DESCARGA LA APP', 'woodmart' );
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php 
        }
        // Updating widget replacing old instances with new
        public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}
// Register the widget
function register_desego_app_list_widget() {
    register_widget( 'Desego_App_List_Widget' );
}
add_action( 'widgets_init', 'register_desego_app_list_widget' );