<?php

class Desego_Tracking_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
         
        // Base ID of your widget
        'desego_tracking_widget', 
         
        // Widget name will appear in UI
        __('Rastreo DESEGO', 'woodmart'), 
         
        // Widget description
        array( 'description' => __( 'Widget de formulario de rastreo para DESEGO', 'woodmart' ), ) );
    }
        
        
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        echo $args['before_widget'];

        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];
        ?>
        <form class="desego-tracking-form" action="" method="">
            <input class="guide-number" type="text" placeholder="Número de guía" />
            <ul>
                <li>
                    <input name="tracking-service" id="track-estafeta" type="radio" value="estafeta" />
                    <label for="track-estafeta">Estafeta</label>
                </li>
                <li>
                    <input name="tracking-service" id="track-redpack" type="radio" value="redpack" />
                    <label for="track-redpack">Red Pack</label>
                </li>
                <li>
                    <input name="tracking-service" id="track-fedex" type="radio" value="fedex" />
                    <label for="track-fedex">FedEx</label>
                </li>
            </ul>
            <a class="button single_add_to_cart_button" href="#" target="_BLANK">BUSCAR GUÍA</a>
        </form>
        <script>
            (function ($) {
                $('.desego-tracking-form a').click(function(e){
                    var guide = $(this).parent().find('.guide-number').val();
                    var service = $(this).parent().find('input[name="tracking-service"]:checked').val();

                    if (guide && service) {
                        switch(service){
                            case "estafeta":
                                url = "https://www.estafeta.com/Herramientas/Rastreo/";
                                break;
                            case "redpack":
                                url = "http://www.redpack.com.mx/";
                                break;                
                            case "fedex":
                                url = "https://www.fedex.com/fedextrack/?tracknumbers=" + guide + "&locale=es_MX&cntry_code=mx";
                                break;
                            default:
                                url="#"
                                break;
                        }
                        $(this).attr('href', url);
                    }else{
                        e.preventDefault();
                    }
                });
            })(jQuery);
        </script>
        <?php
        echo $args['after_widget'];
    }

    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }else {
            $title = __( 'RASTREA TU PEDIDO', 'woodmart' );
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php 
        }
        // Updating widget replacing old instances with new
        public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}
// Register the widget
function register_desego_tracking_widget() {
    register_widget( 'Desego_Tracking_Widget' );
}
add_action( 'widgets_init', 'register_desego_tracking_widget' );