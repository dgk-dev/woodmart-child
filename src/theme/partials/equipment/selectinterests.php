<?php
$product_ids = get_posts(array(
    'post_type'      => array('product'),
    'posts_per_page' => -1,
    'tax_query'      => array(
        'relation' => 'OR',
        array(
            'taxonomy'        => 'pa_tipo',
            'field'           => 'slug',
            'terms'           =>  array('reactivos'),
            'operator'        => 'IN',
        ),
        array(
            'taxonomy'        => 'product_cat',
            'field'           => 'slug',
            'terms'           =>  array('reactivos'),
            'operator'        => 'IN',
        ),
    ),
    'fields' => 'ids'
));

$terms = get_terms(array(
    'taxonomy' => 'pa_categoria',
    'object_ids' => $product_ids
));

//Consulta de lineas de interés
$user_id = get_current_user_id();
$table_name = $wpdb->prefix . "desego_equipment";
$query = $wpdb->get_row( "SELECT interests FROM $table_name WHERE user_id = $user_id");

$selected_interests = $query->interests ? json_decode($query->interests) : array();
?>
<ul class="my-equipment-steps row align-items-start justify-content-between">
    <li class="my-equipment-step">
        <span class="my-equipment-step__num">1</span>
        ÁREA
    </li>
    <li class="my-equipment-step">
        <span class="my-equipment-step__num">2</span>
        PRODUCTOS
    </li>
    <li class="my-equipment-step my-equipment-step--active">
        <span class="my-equipment-step__num">3</span>
        LÍNEAS DE INTERÉS
    </li>
</ul>
<!-- .my-equipment-steps -->

<header class="my-equipment__main-head">
    <!-- TITLE -->
    <div class="wishlist-title wishlist-title--medium ">
        <h2>¿Otra línea de interés?</h2>
    </div>

    <div class="next-btn row justify-content-end">
        <a href="<?php echo wc_get_account_endpoint_url('equipmentDashboard'); ?>" class="button button--rounded button--gray">
            SALTAR
            <svg id="icon-chevron-right" viewBox="0 0 24 24" role="presentation">
                <title>chevron-right</title>
                <path d="M9.707 18.707l6-6c0.391-0.391 0.391-1.024 0-1.414l-6-6c-0.391-0.391-1.024-0.391-1.414 0s-0.391 1.024 0 1.414l5.293 5.293-5.293 5.293c-0.391 0.391-0.391 1.024 0 1.414s1.024 0.391 1.414 0z"></path>
            </svg>
        </a>&nbsp;&nbsp;
        <button class="button button--yellow button--rounded submit-interests">GUARDAR</button>
    </div>
</header>

<form id="selectinterestsForm" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">
    <div class="my-equipment-interests-select">
        <ul class="row justify-content-between my-equipment-area__list">
            <?php foreach($terms as $term): ?>
                <li class="my-equipment-area__item">
                    <input type="checkbox" name="interests[]" id="interest-<?php echo $term->term_id ?>" value="<?php echo $term->term_id ?>" class="hidden" <?php echo (in_array($term->term_id, $selected_interests)) ? 'checked="checked"' : '' ?> />
                    <label for="interest-<?php echo $term->term_id ?>" class="pill">
                        <?php echo $term->name ?>
                        <div class="icon-checkmark">
                            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="8" viewBox="0 0 10 8" role="presentation">
                            <path fill="#FFF" fill-rule="nonzero" d="M1.743 2.38A1 1 0 1 0 .257 3.719l3.024 3.354a1 1 0 0 0 1.51-.028l4.518-5.403A1 1 0 0 0 7.775.359l-3.78 4.52-2.252-2.5z"/>
                        </svg>
                    </div>
                    </label>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <input type="hidden" name="type" value="interests" />
    <input type="hidden" name="action" value="desego_equipment_save_equipment_interests" />
</form>
<!-- .my-equipment-interests-select -->

<div class="next-btn row justify-content-end">
    <a href="<?php echo wc_get_account_endpoint_url('equipmentDashboard'); ?>" class="button button--rounded button--gray">
        SALTAR
        <svg id="icon-chevron-right" viewBox="0 0 24 24" role="presentation">
            <title>chevron-right</title>
            <path d="M9.707 18.707l6-6c0.391-0.391 0.391-1.024 0-1.414l-6-6c-0.391-0.391-1.024-0.391-1.414 0s-0.391 1.024 0 1.414l5.293 5.293-5.293 5.293c-0.391 0.391-0.391 1.024 0 1.414s1.024 0.391 1.414 0z"></path>
        </svg>
    </a>&nbsp;&nbsp;
    <button class="button button--yellow button--rounded submit-interests">GUARDAR</button>
</div>

<script>
jQuery(document).ready(function($){
    var $selectinterestsForm = $('#selectinterestsForm'),
        $submitButton = $('.submit-interests')

    $submitButton.click(function(e){
        e.preventDefault();
        $selectinterestsForm.submit();
    })
});
</script>