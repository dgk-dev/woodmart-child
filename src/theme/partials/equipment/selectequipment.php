<?php
//consulta de equipos
$equipment_args= array(
    'post_type'      => array('product'),
    'post_status'    => 'publish',
    'posts_per_page' => -1,
    'meta_query'     => array(
        array( 
            'key'     => 'in_equipment_module', 
            'value'   => 'yes', 
        )
    ),
    'tax_query'      => array(
        array(
         'taxonomy'        => 'pa_tipo',
         'field'           => 'slug',
         'terms'           =>  array('equipos'),
         'operator'        => 'IN',
        )
    ),
    'fields' => 'ids'
 );

// consulta de categorias
$all_products_ids = get_posts( $equipment_args );
$all_product_categories = wp_get_object_terms($all_products_ids, 'pa_categoria');


//Consulta de equipos guardados
$user_id = get_current_user_id();
$table_name = $wpdb->prefix . "desego_equipment";
$query = $wpdb->get_row( "SELECT equipment FROM $table_name WHERE user_id = $user_id");

$selected_equipment = $query->equipment ? json_decode($query->equipment) : array();

?>
<ul class="my-equipment-steps row align-items-start justify-content-between">
    <li class="my-equipment-step">
        <span class="my-equipment-step__num">1</span>
        ÁREA
    </li>
    <li class="my-equipment-step my-equipment-step--active">
        <span class="my-equipment-step__num">2</span>
        PRODUCTOS
    </li>
    <li class="my-equipment-step">
        <span class="my-equipment-step__num">3</span>
        LÍNEAS DE INTERÉS
    </li>
</ul>
<!-- .my-equipment-steps -->
<div class="my-equipment-products">
    <header class="my-equipment__main-head">
        <!-- TITLE -->
        <div class="wishlist-title wishlist-title--medium ">
            <h2>¿Qué equipos tienes?</h2>
        </div>

        <div class="my-equipment-filter row align-items-center justify-content-between">
            <div class="form-group row justify-content-start">
                <select class="select2" id="area-select" name="area" data-placeholder="Todas las áreas">
                    <option value>---Todos---</option>
                    <option value="humano">Humano</option>
                    <option value="veterinario">Veterinario</option>
                </select>
                <select class="select2" id="category-select" name="category" data-placeholder="Ver categorías">
                    <option value>---Todas---</option>
                    <?php foreach($all_product_categories as $category): ?>
                        <option value="<?php echo $category->slug; ?>"><?php echo $category->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="next-btn row justify-content-end">
                <a href="<?php echo wc_get_account_endpoint_url('equipmentSelectinterests'); ?>" class="button button--rounded button--gray">
                    SALTAR
                    <svg id="icon-chevron-right" viewBox="0 0 24 24" role="presentation">
                        <title>chevron-right</title>
                        <path d="M9.707 18.707l6-6c0.391-0.391 0.391-1.024 0-1.414l-6-6c-0.391-0.391-1.024-0.391-1.414 0s-0.391 1.024 0 1.414l5.293 5.293-5.293 5.293c-0.391 0.391-0.391 1.024 0 1.414s1.024 0.391 1.414 0z"></path>
                    </svg>
                </a>&nbsp;&nbsp;&nbsp;
                <button class="button button--yellow button--rounded submit-equipment">GUARDAR</button>
            </div>
        </div>
    </header>
    <form id="selectequipmentForm" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">
        <div class="products elements-grid align-items-start woodmart-products-holder  woodmart-spacing-30 row grid-columns-3">
            <!-- Carga con ajax -->
        </div>
        <input type="hidden" name="type" value="equipment" />
        <input type="hidden" name="selected-equipment" value='<?php echo json_encode($selected_equipment) ?>' />
        <input type="hidden" name="action" value="desego_equipment_save_equipment_interests" />
        <div class="woodmart-shop-loader custom"></div>
        <!-- .products elements-grid -->
        
        <div class="products-footer">
            <button id="load-more-button" class="btn woodmart-load-more"><span class="load-more-label">Más productos</span><span class="load-more-loading">Cargando...</span></button>
        </div>

    </form>
</div>
<!-- .my-equipment-products -->


<div class="next-btn row justify-content-end">
    <a href="<?php echo wc_get_account_endpoint_url('equipmentSelectinterests'); ?>" class="button button--rounded button--gray">
        SALTAR
        <svg id="icon-chevron-right" viewBox="0 0 24 24" role="presentation">
            <title>chevron-right</title>
            <path d="M9.707 18.707l6-6c0.391-0.391 0.391-1.024 0-1.414l-6-6c-0.391-0.391-1.024-0.391-1.414 0s-0.391 1.024 0 1.414l5.293 5.293-5.293 5.293c-0.391 0.391-0.391 1.024 0 1.414s1.024 0.391 1.414 0z"></path>
        </svg>
    </a>&nbsp;&nbsp;
    <button class="button button--yellow button--rounded submit-equipment">GUARDAR</button>
</div>

<script>
jQuery(document).ready(function($){
    var $equipmentForm = $('#selectequipmentForm'),
        $submitButton = $('.submit-equipment'),
        $areaSelect = $('#area-select'),
        $categorySelect = $('#category-select'),
        $productGrid = $('.products.elements-grid'),
        $loadMoreButton = $('#load-more-button'),
        $selectedEquipment = $('input:hidden[name="selected-equipment"]'),
        pagination = 0,
        doingAjax = false,
        xhr;

    $areaSelect.add($categorySelect).change(function(){
        if(xhr && xhr.readyState != 4){
            xhr.abort();
        }
        loadEquipment('reload');
    });

    $loadMoreButton.click(function(e){
        e.preventDefault();
        if(!doingAjax) loadEquipment('more');
    });

    $submitButton.click(function(e){
        e.preventDefault();
        $equipmentForm.submit();
    });

    var checkedEquipment = JSON.parse($selectedEquipment.val());
    console.log(checkedEquipment);
    $productGrid.on('click', 'input:checkbox[name="equipment[]"]', function(){
        var value = $(this).val();

        if($(this).is(":checked")) {
            checkedEquipment.push(value);
        }else{
            index = checkedEquipment.indexOf(value);
            if (index > -1) checkedEquipment.splice(index, 1);
        }

        $selectedEquipment.val(JSON.stringify(checkedEquipment));
    });

    function loadEquipment(internalAction){
        if(internalAction=='reload') pagination=1;
        if(internalAction=='more') pagination++;

        xhr = $.ajax({
			type: 'post',
			url: '<?php echo admin_url("admin-ajax.php") ?>',
			data: {
				area: $areaSelect.val(),
                category: $categorySelect.val(),
                paged: pagination,
                action: 'desego_load_equipment',
			},
			success: function(response){
				var data = $(response);
				if(data.length){
                    $productGrid.append(data);
                }
			},
			error: function(response){
				console.log('error', response);
			}, 
			beforeSend: function (qXHR, settings) {
				if(internalAction=='reload'){
                    $loadMoreButton.hide();
                    $productGrid.empty();
                    $equipmentForm.addClass('ajax-loading ajax-loading-custom');
                }

                if(internalAction=='more') $loadMoreButton.addClass('loading');
                
                doingAjax = true;
			},
			complete: function () {
                if(internalAction=='reload'){
                    $loadMoreButton.show();
                    $equipmentForm.removeClass('ajax-loading ajax-loading-custom');
                }
                
                $('input:checkbox[name="equipment[]"]').each(function(){
                    if(checkedEquipment.includes(parseInt($(this).val()))){
                        $(this).attr('checked', 'checked');
                    }
                });

                if (pagination >= $('#max-pages').val()) $loadMoreButton.hide();
                if(internalAction=='more') $loadMoreButton.removeClass('loading');

                if (typeof woodmartThemeModule !== 'undefined' ) woodmartThemeModule.lazyLoading();

                doingAjax = false;
			}
		});
    }

    loadEquipment('reload');
});
</script>