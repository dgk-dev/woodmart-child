<?php
    $thumbnail_id = get_post_thumbnail_id(get_the_ID());
    $image = wp_get_attachment_image($thumbnail_id, 'woocommerce_thumbnail');
?>
<div class="product-grid-item product col-6 col-sm-6 col-md-4 col-lg-4 product-in-grid has-post-thumbnail">

    <input type="checkbox" class="hidden" name="equipment[]" id="eq-<?php echo get_the_ID(); ?>" value="<?php echo get_the_ID(); ?>" />
    <div class="product__griditem">
        <div class="icon-checkmark">
            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="8" viewBox="0 0 10 8" role="presentation">
                <path fill="#FFF" fill-rule="nonzero" d="M1.743 2.38A1 1 0 1 0 .257 3.719l3.024 3.354a1 1 0 0 0 1.51-.028l4.518-5.403A1 1 0 0 0 7.775.359l-3.78 4.52-2.252-2.5z"/>
            </svg>
        </div>
        
        <label for="eq-<?php echo get_the_ID(); ?>">
            <div class="product-element-top">
                <?php echo $image; ?> 
            </div>

            <h3 class="product-title"><?php the_title(); ?></h3>
        </label>
    </div>
</div>