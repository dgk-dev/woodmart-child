<?php
if(!is_user_logged_in()) return;
$video_watched = isset($_COOKIE['videopopup_watched']) ? $_COOKIE['videopopup_watched'] : 0;
?>
<script>
jQuery(document).ready(function($){
    function setVideoWatched(){
        $.ajax({
            type: 'post',
            url: '<?php echo admin_url("admin-ajax.php") ?>',
            data: {
                action: 'desego_set_video_watched',
            },
            success: function(response){
                //success
            },
            error: function(response){
                console.log('error', response);
            },
        });
    }

    var popupArgs = {
        items: {
            src: 'https://vimeo.com/296302547'
        },
        type: 'iframe',
        mainClass: 'video-popup',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: true,
        callbacks: {
            close: function(){
                setVideoWatched();
            }
        }
    };

    <?php if(!$video_watched): ?> $.magnificPopup.open(popupArgs); <?php endif; ?>

    $('#popup-video').click(function(e){
        e.preventDefault();
        $(this).magnificPopup(popupArgs).magnificPopup('open');
    });
});

</script>