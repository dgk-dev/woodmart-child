<?php
use XTS\WC_Wishlist\Wishlist;
$user_args = array(
    'number' => -1,
    'fields' => 'ID'
);

$users = get_users($user_args);

$yith_product_args = array(
    'wishlist_visibility' => 'all',
    'wishlist_id' => 'all'
);
foreach($users as $user){
    /**
     * CAmbiar wishlists
     */
    $yith_product_args['user_id'] = $user;
    $user_products = YITH_WCWL()->get_products($yith_product_args);
    if($user_products){
        $wishlist = new Wishlist(false, $user);
        foreach($user_products as $product){
            $wishlist->add($product['ID']);
            echo "user: ".$user.' ';
            echo "product: ".$product['ID'].'<br>';
        }
    }

    /**
     * Preaprobar usuarios (azexo email verification)
     */

    update_user_meta($user, 'user-approved', true);
}