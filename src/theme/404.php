<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header(); ?>

<div class="site-content col-12" role="main">

	<header class="page-header">
		<h3 class="page-title"><?php esc_html_e( 'PÁGINA NO ENCONTRADA', 'woodmart' ); ?></h3>
	</header>

	<div class="page-wrapper">
		<div class="page-content">
			<h2><?php esc_html_e( 'Lo sentimos, pero la página que busca no existe.', 'woodmart' ); ?></h2>
			<p><?php esc_html_e( 'Por favor verifique la dirección introducida e inténtelo de nuevo. También puede realizar una búsqueda.', 'woodmart' ); ?></p>

			<?php
				woodmart_search_form();
			?>
		</div><!-- .page-content -->
	</div><!-- .page-wrapper -->

</div><!-- .site-content -->

<?php get_footer(); ?>