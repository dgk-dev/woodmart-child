<?php
/**
 * Product Loop End
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

//Check if reactivos
$reactivos_category = is_product_category( 'reactivos' );
?>
<?php if(!$reactivos_category): ?>
    </div>
<?php else: ?>
    </table>
<?php endif; ?>