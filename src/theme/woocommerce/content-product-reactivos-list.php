<?php 
	global $product;


	do_action( 'woocommerce_before_shop_loop_item' ); 
?>
    <td data-label="SKU">
        <?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
            <?php $sku = $product->get_sku(); ?>
            <span class="sku_wrapper"><span class="sku"><?php echo true == $sku ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>
        <?php endif; ?>
    </td>

    <td data-label="Producto">
        <?php
           the_title();
        ?>
    </td>

    <td data-label="Precio">
        <?php
            /**
             * woocommerce_after_shop_loop_item_title hook
             *
             * @hooked woocommerce_template_loop_rating - 5
             * @hooked woocommerce_template_loop_price - 10
             */
            do_action( 'woocommerce_after_shop_loop_item_title' );
        ?>
    </td>

    <td class="text-center quantity-cell" data-label="Cantidad">
        <div class="woodmart-add-btn wd-add-btn-replace"><?php do_action( 'woocommerce_after_shop_loop_item' ); ?></div>
    </td>