��          �       <      <     =     M     e  +   ~  M   �     �  -     .   <     k     p     �  
   �  ,   �  !   �     �       %   "  p  H     �      �     �  4     \   C     �  )   �  )   �               "     =  .   I     x     �     �     �   Add new address Add new billing address Add new shipping address Billing address used for the previous order Identifier / Name (Examples: "Office address," "Mary Jones," "MJ 2145," etc.) Latest Used Addresses Make this address the default billing address Make this address the default shipping address Save Select a country Select an address Select one Shipping address used for the previous order There are no additional addresses VAT Identification Number VAT Identification Number: placeholderVAT Identification Number Plural-Forms: nplurals=2; plural=n != 1;
Project-Id-Version: WooCommerce Multiple Customer Addresses
POT-Creation-Date: 2019-04-18 10:50+0200
PO-Revision-Date: 2019-04-23 19:28+0000
Last-Translator: Ricardo Davila <admindesego@desego.com>
Language-Team: Español
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woocommerce-multiple-customer-addresses.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
Report-Msgid-Bugs-To: 
Language: es_ES
X-Loco-Version: 2.2.2; wp-5.1.1 AGREGAR NUEVA DIRECCIÓN NUEVA DIRECCIÓN DE FACTURACIÓN NUEVA DIRECCIÓN DE ENVÍO Dirección de facturación usada en órdenes previas Identificador / Nombre (Ejemplos: "Dirección de la oficina", "Mary Jones", "MJ 2145", etc.) Últimas direcciones utilizadas Establecer dirección como predeterminada Establecer dirección como predeterminada Guardar País Seleccione una dirección
 Seleccionar Dirección de envío usada en órdenes previas No hay direcciones adicionales RFC RFC Ingresar RFC 