<?php
/*
Plugin Name: BBVA Bancomer Gateway for WooCommerce (UPDATED for DESEGO v1.0.0)
Plugin URI: http://www.codection.com
Description: This plugins allows to users to include BBVA - Bancomer in their WooCommerce installations
Author: codection
Version: 1.0.1
Author URI: https://codection.com
*/

add_action( 'plugins_loaded', 'bbva_bancomer_plugins_loaded' );
add_action( 'init', 'bbva_bancomer_inicio' );

function bbva_bancomer_inicio() {
	load_plugin_textdomain( "bbva_bancomer_gw_woo", false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

function bbva_bancomer_plugins_loaded() {
	if ( !class_exists( 'WC_Payment_Gateway' ) ) exit;

	include_once ('class-wc-bbva-bancomer-gateway.php');
	
	add_filter( 'woocommerce_payment_gateways', 'woocommerce_add_gateway_bbva_bancomer_gateway' );
}

function woocommerce_add_gateway_bbva_bancomer_gateway($methods) {
	$methods[] = 'WC_BBVA_Bancomer_Gateway';
	return $methods;
}

function bbva_bancomer_enqueue($hook) {
	if ( 'woocommerce_page_wc-settings' != $hook ) {
        return;
    }

    wp_enqueue_script( 'jquery-validate', plugin_dir_url( __FILE__ ) . 'js/jquery.validate.min.js', array( "jquery" ), "1.13.1", true );
}
add_action( 'admin_enqueue_scripts', 'bbva_bancomer_enqueue' );

function bbva_bancomer_plugin_row_meta( $links, $file ){
	if ( strpos( $file, basename( __FILE__ ) ) !== false ) {
		$new_links = array(
					'<a href="https://www.paypal.me/codection" target="_blank">' . __( 'Donate', "bb_gw_woo" ) . '</a>'
				);
		
		$links = array_merge( $links, $new_links );
	}
	
	return $links;
}
add_filter('plugin_row_meta', 'bbva_bancomer_plugin_row_meta', 10, 2);

function bbva_write_ipn_log( $mensaje )  {
	if ( is_array( $mensaje ) || is_object( $mensaje ) ) {
		$mensaje = print_r( $log, true );
	}

    $gestor = fopen( plugin_dir_path( __FILE__ ) . "ipn.log", 'a' );
    fwrite( $gestor, date( 'Y-m-d H:i:s' ) . " --- " . $mensaje . PHP_EOL );
    fclose( $gestor );
}

