<?php

class WC_BBVA_Bancomer_Gateway extends WC_Payment_Gateway {

	public function __construct() {
		$this->meses = array( 3, 6, 12, 18 );
		
		$this->id				= 'bbva_bancomer';
		$this->icon 			= apply_filters('woocommerce_bbva_bancomer_icon', plugin_dir_url( __FILE__ ) . '/tarjetas.png');
		$this->has_fields 		= false;
		$this->method_title     = __( 'BBVA Bancomer', "bbva_bancomer_gw_woo" );

		$this->init_form_fields();
		$this->init_settings();

		$this->title 			= apply_filters( 'woobbva_bancomer_title', $this->get_option( 'title' ) );
		$this->description      = apply_filters( 'woobbva_bancomer_description', $this->get_option( 'description' ) );
		
		$this->commerce 		= $this->get_option( 'commerce' );
		$this->terminal 		= $this->get_option( 'terminal' );
		$this->key 				= $this->get_option( 'key' );
		$this->signature 		= $this->get_option( 'signature' );
		$this->merchantName 	= $this->get_option( 'merchantName' );
		$this->owner 			= $this->get_option( 'owner' );
		$this->after_payment	= $this->get_option( 'after_payment' );
		$this->forceHTTPS		= $this->get_option( 'forceHTTPS' );
		$this->debug_post_send	= $this->get_option( 'debug_post_send' );
		$this->debug_ipn_reception	= $this->get_option( 'debug_ipn_reception' );
		$this->pago_aplazado	= $this->get_option( 'pago_aplazado' );

		foreach ( $this->meses as $mes ) {
			$this->{'recargo_'.$mes}	= $this->get_option( 'recargo_' . $mes );	
		}
		
		if( $this->forceHTTPS == "http" )
			$this->notify_url 		= str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'WC_BBVA_Bancomer_Gateway', home_url( '/' ) ) );
		else
			$this->notify_url 		= str_replace( 'http:', 'https:', add_query_arg( 'wc-api', 'WC_BBVA_Bancomer_Gateway', home_url( '/' ) ) );

		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_receipt_bbva_bancomer', array( $this, 'receipt_page' ) );
		add_action( 'woocommerce_api_' . strtolower( get_class( $this ) ), array( $this, 'bbva_bancomer_ipn_response') );
		add_filter( 'woocommerce_after_checkout_billing_form' , array( $this, 'after_checkout_billing_form' ) );
		add_filter( 'woocommerce_after_checkout_form' , array( $this, 'javascript_checkout' ) );
		add_action( 'woocommerce_cart_calculate_fees', array( $this, 'calculate_fee' ) );
		add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'checkout_update_order_meta' ) );
	}

	function bbva_bancomer_ipn_response(){
		global $woocommerce;

		if( $this->debug_ipn_reception == 'yes' ){
			bbva_write_ipn_log( "Se recibe una comunicación del banco" );
			bbva_write_ipn_log( $_POST );	
		}	
	
		if ( empty( $_REQUEST ) || empty( $_POST ) ) {
			wp_die( 'Fallo en el proceso de pago.<br>Su pedido ha sido cancelado.' );
			return;
		}

		$post_filtered = filter_input_array( INPUT_POST );
		
		if ( $post_filtered['Ds_Response'] == '000' ):
			$order_id = intval( substr( $post_filtered['Ds_Order'], 0, 8 ) );

			if( $this->debug_ipn_reception == 'yes' ){
				bbva_write_ipn_log( "Se ha recibido un pago el pedido: " . $order_id );
			}

			$order = new WC_Order( $order_id );

			if ( $order->status == 'completed' )
				exit;

			$virtual_order = null;
 
			if ( count( $order->get_items() ) > 0 ) {
				foreach( $order->get_items() as $item ) {
					if ( 'line_item' == $item['type'] ) {
						$_product = $order->get_product_from_item( $item );

						if ( ! $_product->is_virtual() ) {
							$virtual_order = false;
							break;
						} else {
							$virtual_order = true;
						}	
					}
				}
			}

			$downloadable_order = null;
 
			if ( count( $order->get_items() ) > 0 ) {
				foreach( $order->get_items() as $item ) {
					if ( 'line_item' == $item['type'] ) {
						$_product = $order->get_product_from_item( $item );

						if ( ! $_product->is_virtual() ) {
							$downloadable_order = false;
							break;
						} else {
							$downloadable_order = true;
						}	
					}
				}
			}

			// choose between options
			switch( $this->after_payment ){
				case "completed":
					$order->update_status( 'completed' );
				break;

				case "processing":
					$order->update_status( 'processing' );
				break;

				case "completed_downloadable":
					if( $downloadable_order )
						$order->update_status( 'completed' );
					else
						$order->update_status( 'processing' );
				break;

				case "completed_virtual":
					if( $virtual_order )
						$order->update_status( 'completed' );
					else
						$order->update_status( 'processing' );
				break;

				case "completed_downloadable_virtual":
					if( $downloadable_order || $virtual_order )
						$order->update_status( 'completed' );
					else
						$order->update_status( 'processing' );
				break;
			}

			$order->reduce_order_stock();
			$woocommerce->cart->empty_cart();
			$order->add_order_note( sprintf( __( 'BBVA Bancomer order completed, code %s', "bbva_bancomer_gw_woo" ), $post_filtered['Ds_AuthorisationCode'] ) );
		else:
			$order = new WC_Order( $post_filtered['Ds_Order'] );

			$order->update_status('cancelled');
			
			$order->add_order_note( sprintf( __( 'BBVA Bancomer payment error, code %s', "bbva_bancomer_gw_woo" ), $post_filtered['Ds_ErrorCode'] ) );	
		endif;
	}
	
	function init_form_fields() {
		$fields = array(
				'enabled' => array(
						'title' => __( 'Enable/Disable', "bbva_bancomer_gw_woo" ),
						'type' => 'checkbox',
						'label' => __( 'Enable BBVA Bancomer', "bbva_bancomer_gw_woo" ),
						'default' => 'yes'
				),
				'title' => array(
						'title' => __( 'Title', "bbva_bancomer_gw_woo" ),
						'type' => 'text',
						'description' => __( 'This title is showed in checkout process.', "bbva_bancomer_gw_woo" ),
						'default' => __( 'BBVA Bancomer', "bbva_bancomer_gw_woo" ),
						'desc_tip' => true,
				),
				'description' => array(
						'title' => __( 'Description', "bbva_bancomer_gw_woo" ),
						'type' => 'textarea',
						'description' => __( 'Description of the method of payment. Use it to tell the user that it is a secure system through bank.', "bbva_bancomer_gw_woo" ),
						'default' => __( 'Secure payment by credit card. You will be redirected to the secure website of the bank.', "bbva_bancomer_gw_woo" )
				),
				'owner' => array(
						'title' => __( 'Owner', "bbva_bancomer_gw_woo" ),
						'type' => 'text',
						'default' => ''
				),
				'merchantName' => array(
						'title' => __( 'Trade name', "bbva_bancomer_gw_woo" ),
						'type' => 'text',
						'default' => ''
				),
				'commerce' => array(
						'title' => __( 'Trade number', "bbva_bancomer_gw_woo" ),
						'type' => 'text',
						'default' => ''
				),
				'terminal' => array(
						'title' => __( 'Terminal number', "bbva_bancomer_gw_woo" ),
						'type' => 'text',
						'default' => '1'
				),
				'key' => array(
						'title' => __( 'Secret key', "bbva_bancomer_gw_woo" ),
						'type' => 'text',
						'description' => __('Encryptation Secret Key.', "bbva_bancomer_gw_woo" ),
						'default' => ''
				),
				'after_payment' => array(
						'title' => __( 'What to do after payment is done?', "bbva_bancomer_gw_woo" ),
						'type'	=> 'select',
						'options' => array(
								'processing' => __( 'Always processing', "bbva_bancomer_gw_woo" ),
								'completed_downloadable' => __( 'Pending except if all products are downloadable, in this case it would be marked as completed', "bbva_bancomer_gw_woo" ),
								'completed_virtual' => __( 'Pending except if all products are virtual, in this case it would be marked as completed', "bbva_bancomer_gw_woo" ),
								'completed_downloadable_virtual' => __( 'Pending except if all products are downloadable or virtual, in this case it would be marked as completed', "bbva_bancomer_gw_woo" ),
								'completed' => __( 'Always completed', "bbva_bancomer_gw_woo" )
						),
						'description' => __( 'After payment, how the order should be marked?', "bbva_bancomer_gw_woo" ),
				),
				'forceHTTPS' => array(
						'title' => __( 'Force HTTPS answer', "redsys_gw_woo" ),
						'type'	=> 'select',
						'default' => 'http',
						'options' => array(
								'http' => __( 'No, use the standard answer using HTTP', "redsys_gw_woo" ),
								'https' => __( 'Yes, force the answer using HTTPS', "redsys_gw_woo" )
						),
						'description' => __( 'If the system is working leave the standard mode, if you are using HTTPS and you have problems when the orders are payed but they are not marked as completed or processing, try to force HTTPS', "redsys_gw_woo" )
				),
				'debug_post_send' => array(
						'title' => __( 'Do you want to debug POST data on sending?', "bbva_bancomer_gw_woo" ),
						'type'	=> 'select',
						'options' => array(
								'no' => __( 'No', "bbva_bancomer_gw_woo" ),
								'yes' => __( 'Yes', "bbva_bancomer_gw_woo" )
						),
						'description' => __( 'It is only for debugging, if activated, data will never be sent to bank.', "bbva_bancomer_gw_woo" ),
				),
				'debug_ipn_reception' => array(
						'title' => __( 'Do you want to debug IPN bank data on reception?', "bbva_bancomer_gw_woo" ),
						'type'	=> 'select',
						'options' => array(
								'no' => __( 'No', "bbva_bancomer_gw_woo" ),
								'yes' => __( 'Yes', "bbva_bancomer_gw_woo" )
						),
						'description' => __( 'It is only for debugging, if activated, all data transferred to the bank will be saved in the file: ', "bbva_bancomer_gw_woo" ) . '<a href="' . plugin_dir_url( __FILE__ ) . 'ipn.log">ipn.log</a>' ,
				),
				'pago_aplazado' => array(
						'title' => 'Permitir pago aplazado',
						'type' => 'checkbox',
						'label' => 'Si permite el pago aplazado, recuerde rellenar abajo los % de recargo, si no quiere aplicar recargo, déjelos a 0',
						'default' => 'no'
				),
		);
		
		$meses_fields = array();
		foreach ( $this->meses as $mes) {
			$meses_fields['recargo_' . $mes] = array(
				'title' => "Recargo para $mes meses",
				'type' => 'text',
				'description' => "Indique el porcentaje de recargo que se aplicará para el pago en $mes meses",
				'default' => '0'
			);
		}

		$this->form_fields = array_merge( $fields, $meses_fields );
	}

	public function admin_options() {
		?>
		<h3><?php _e( 'BBVA Bancomer Payment', "bbva_bancomer_gw_woo" ); ?></h3>
		<p><?php _e('Allows BBVA Bancomer card payments.', "bbva_bancomer_gw_woo" ); ?></p>
		<table class="form-table">
			<?php $this->generate_settings_html(); ?>
		</table>

		<script>
		jQuery( document ).ready( function( $ ){
			$.validator.addMethod("requiredIfChecked", function (val, ele, arg) {
			    if ($("#woocommerce_bbva_bancomer_enabled").is(":checked") && ($.trim(val) == '')) { return false; }
			    return true;
			}, "This field is required if gateway is enabled");


			$("#mainform").validate({
				rules: {
					woocommerce_bbva_bancomer_title: "requiredIfChecked",
					woocommerce_bbva_bancomer_description: "requiredIfChecked",
					woocommerce_bbva_bancomer_owner: "requiredIfChecked",
					woocommerce_bbva_bancomer_merchantName: "requiredIfChecked",
					woocommerce_bbva_bancomer_commerce: "requiredIfChecked",
					woocommerce_bbva_bancomer_terminal: "requiredIfChecked",
					woocommerce_bbva_bancomer_key: "requiredIfChecked"
				},

				messages: {
					woocommerce_bbva_bancomer_title: "<?php _e( 'You must fill out a title for this gateway', 'bbva_bancomer_gw_woo' ); ?>",
					woocommerce_bbva_bancomer_description: "<?php _e( 'You must fill out a description for this gateway', 'bbva_bancomer_gw_woo' ); ?>",
					woocommerce_bbva_bancomer_owner: "<?php _e( 'You must fill out who is the owner', 'bbva_bancomer_gw_woo' ); ?>",
					woocommerce_bbva_bancomer_merchantName: "<?php _e( 'You must fill out the merchant name', 'bbva_bancomer_gw_woo' ); ?>",
					woocommerce_bbva_bancomer_commerce: "<?php _e( 'You must fill out the merchant number', 'bbva_bancomer_gw_woo' ); ?>",
					woocommerce_bbva_bancomer_terminal: "<?php _e( 'You must fill out the terminal number. If you don not know it, it probably would be the number: 1', 'bbva_bancomer_gw_woo' ); ?>",
					woocommerce_bbva_bancomer_key: "<?php _e( 'You must fill out the key', 'bbva_bancomer_gw_woo' ); ?>"
				}
			});			
		} )
		</script>
		<?php
	}

	function receipt_page( $order ) {
		echo '<p>'.__( 'Thank you for your order, click on the button to pay with BBVA Bancomer.', "bbva_bancomer_gw_woo" ).'</p>';
		echo $this->generate_bbva_bancomer_form( $order );
	}

	public function generate_bbva_bancomer_form( $order_id ) {
		global $woocommerce;

		$order = new WC_Order( $order_id );

		bbva_write_ipn_log( "Se va a enviar el pago para el pedido " . $order_id );

		$gateway_address = 'https://ecom.eglobal.com.mx/VPBridgeWeb/servlets/TransactionStartBridge';
		
		$bbva_bancomer_args = $this->prepare_args( $order );
		$bbva_bancomer_args_array = array();

		foreach ($bbva_bancomer_args as $key => $value) {
			$bbva_bancomer_args_array[] = '<input type="hidden" name="'.esc_attr( $key ).'" value="'.esc_attr( $value ).'" /><br/>';
		}

		if( $this->debug_post_send == 'yes' ){
			echo "<h1>Depuración activada, datos POST a enviar:</h1>";
			echo "<pre>";
			var_dump( $bbva_bancomer_args );
			echo "</pre>";
			die();
		}

		wc_enqueue_js( '
			jQuery("body").block({
				message: "'.__( 'Thank you for your order. We are now redirecting you to BBVA Bancomer to make payment.', "bbva_bancomer_gw_woo" ).'",
				overlayCSS:
				{
					background: "#fff",
					opacity: 0.6
				},
				css: {
					padding:        20,
					textAlign:      "center",
					color:          "#555",
					border:         "3px solid #aaa",
					backgroundColor:"#fff",
					cursor:         "wait",
					lineHeight:		"32px"
				}
			});
			jQuery("input[name=Ds_Merchant_TransactionType]").val(0);
			jQuery("#submit_bbva_bancomer_payment_form").removeAttr("disabled");
			jQuery("#submit_bbva_bancomer_payment_form").click();
		' );
	 	
		return '<form action="'.esc_url( $gateway_address ).'" method="post" id="bbva_bancomer_payment_form" target="_top">
			' . implode( '', $bbva_bancomer_args_array) . '
			<input type="submit" class="button-alt" id="submit_bbva_bancomer_payment_form" value="'.__( 'Pay via BBVA Bancomer', "bbva_bancomer_gw_woo" ).'" disabled="disabled "/> <a class="button cancel" href="'.esc_url( $order->get_cancel_order_url() ).'">'.__( 'Cancel order &amp; restore cart', "bbva_bancomer_gw_woo" ).'</a>
			</form>';

	}

	function prepare_args( $order ) {
		global $woocommerce;


		$order_id = $order->get_id();
		$ds_order = str_pad($order_id, 8, "0", STR_PAD_LEFT) . date('is');
		
		$message =  $order->get_total()*100 .
			$ds_order .
			$this->commerce .
			"484" .
			"0" .
			$this->key;
				
		$signature = strtolower( sha1( $message ) );
		
		$args = array (
				'Ds_Merchant_MerchantCode'			=> $this->commerce,
				'Ds_Merchant_Terminal'				=> $this->terminal,
				'Ds_Merchant_Currency'				=> 484,
				'Ds_Merchant_MerchantURL'			=> $this->notify_url,
				'Ds_Merchant_TransactionType'		=> 0,
				'Ds_Merchant_MerchantSignature'		=> $signature,
				'Ds_Merchant_UrlKO'					=> apply_filters( 'woobbva_bancomer_param_urlKO', $order->get_checkout_payment_url() ),
				'Ds_Merchant_UrlOK'					=> apply_filters( 'woobbva_bancomer_param_urlOK', $this->get_return_url( $order ) ),
				'Ds_Merchant_Titular'				=> $this->owner,
				'Ds_Merchant_MerchantName'			=> $this->merchantName,
				'Ds_Merchant_Amount'				=> round($order->get_total()*100),
				'Ds_Merchant_ProductDescription'	=> sprintf( __( 'Order %s' , "bbva_bancomer_gw_woo" ), $order->get_order_number() ),
				'Ds_Merchant_Order'					=> $ds_order,
		);

		// pago aplazado
		$pago_aplazado = get_post_meta( $order_id, 'pago_aplazado', true );
		if( !empty( $pago_aplazado ) && $pago_aplazado != 'contado' ){
			$meses = substr( $pago_aplazado, 8 );
			$args['Ds_Promo_Parcializacion'] = str_pad( $meses, 2, "0", STR_PAD_LEFT );
			$args['Ds_Promo_TipoPlan'] = '03';
		}

		return $args;		
	}

	function process_payment( $order_id ) {
    	$order = new WC_Order( $order_id );

    	return array(
			'result' 	=> 'success',
			'redirect'	=> add_query_arg('order', $order->get_id(), add_query_arg('key', $order->get_order_key(), $this->get_return_url($order)))
		);
    }

    function after_checkout_billing_form( $checkout ) {
    	if( $this->pago_aplazado != 'yes' )
    		return;

    	$opciones['contado'] = 'Pago al contado';
    	foreach ( $this->meses as $mes ) {
			$opciones['recargo_' . $mes] = "Aplazar en $mes meses";    		
    	}

	    echo '<div id="message_fields">';
		woocommerce_form_field( 'pago_aplazado', array(
			'type' => 'select',
			'label' => 'Pago aplazado',
			'placeholder' => 'Indique si quiere aplazar el pago',
			'class' => array('form-row-wide'),
			'options'       => $opciones
		), $checkout->get_value( 'pago_aplazado' ));
		echo '</div>';
	}

	function javascript_checkout(){
		?>
		<script type="text/javascript">
			jQuery( document ).ready( function( $ ){
				$( '#pago_aplazado' ).select2( { width: '100%' } );

				$( '#pago_aplazado' ).change( function(){
					$( document.body ).trigger('update_checkout');
				});
			} ) 
		</script>
		<?php
	}

	function ajax_pago_aplazado() {
		check_ajax_referer( 'codection-security', 'security' );

	    if( isset( $_POST['pago_aplazado'] ) ) {
	        session_start();
	        $_SESSION['pago_aplazado'] = $_POST['pago_aplazado'];
	        echo '1';
	        wp_die();
	    }

	    echo '0';
	    wp_die();
	}

	function calculate_fee() {
		if( $this->pago_aplazado != 'yes' )
			return;

		if( ! $_POST || ( is_admin() && ! is_ajax() ) )
			return;
		
		if( isset( $_POST['post_data'] ) ) {
			parse_str( $_POST['post_data'], $post_data );
		} else {
			$post_data = $_POST;
		}

		switch ( $post_data['pago_aplazado'] ) {
			case 'contado':
				return;
				break;
			
			default:
				$recargo = $this->{$post_data['pago_aplazado']};
				$surcharge = ( WC()->cart->cart_contents_total + WC()->cart->shipping_total ) * $recargo/100;
				WC()->cart->add_fee( 'Recargo por pago aplazado', $surcharge, true, '' );
				break;
		}	 
	}

	function checkout_update_order_meta( $order_id ) {
		if ( ! empty( $_POST['pago_aplazado'] ) ) {
			update_post_meta( $order_id, 'pago_aplazado', sanitize_text_field( $_POST['pago_aplazado'] ) );
		}
	}
}