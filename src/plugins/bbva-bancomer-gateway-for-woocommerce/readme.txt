=== BBVA Bancomer Gateway for WooCommerce ===
Contributors: carazo, hornero
Donate link: http://paypal.me/codection
Tags: woocommerce, bbva_bancomer, credit card, martercard, visa, ecommerce
Requires at least: 3.5
Tested up to: 4.6
Stable tag: 0.9.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Integración para WooCommerce del sistema de pago por tarjeta BBVA Bancomer para México.

== Description ==
Con este plugin podemos usar la pasarela de pago BBVA Bancomer México con WooCommerce. El plugin incorpora un formulario de configuración para sólo tener que introducir los datos que nos ofrezcan desde el banco. Este formulario, tiene una validación incorporada para facilitar su uso y minimizar errores.

Algunas características interesantes:

*   El plugin automáticamente marca el pedido como completado si el pago se realiza de forma satisfactoria.
*   Preparado para internacionalización
*   Certificado para WPML
*	Compatible con Polylang

The description of this plugin is in Spanish because of BBVA Bancomer México is a system from México. If need anything about it in English, tell us directly at contacto AT codection DOT com.

== Installation ==

Existen dos métodos de instalación:

1. Descomprima el archivo, y copie la carpeta 'bbva-bancomer-gateway-for-woocommerce' en su carpeta de plugins del servidor (/wp-content/plugins/).
2. Active el plugin desde el menú de Plugins.

O con el fichero sin descomprimir directamente en "Plugins", "Añadir nuevo", use la herramienta para "Subir plugin" y posteriormente actívelo.

== Preguntas frecuentes (FAQs) ==

= Configuración =

1. Nos aseguramos en Plugins que este plugin está activado
2. Nos dirigimos al menú, concretamente a WooCommerce -> Ajustes -> Finalizar compra, arriba veremos BBVA Bancomer, hacemos clic sobre ese enlace
3. Rellenamos el formulario

= ¿Cómo consigo una pasarela de pago para que mis clientes paguen con tarjeta? =
Debe hablar este tema directamente con su banco. Una vez contratada la pasarela le enviarán los datos necesarios, los introducierá en el formulario de configuración y el plugin estará listo para funcionar.

Lea detenidamente las condiciones de la pasarela y aparte, también el correo que le envíen, los bancos suelen activar las pasarelas en pruebas para posteriormente pasarlas a producción.

== Screenshots ==

1. Formulario de datos de la configuración de WooCommerce
2. Pasarela de pago en el front-end en la selección del proceso de pago.

== Changelog ==

= 1.0.0 =
* Posibilidad de ofrecer pago por aplazado.

= 0.9.3 =
* Posibilidad de forzar la respuesta por HTTP/HTTPS para evitar problemas del SSL.

= 0.9.2 =
* Posibilidad de depurar envío y recepción de datos.

= 0.9.1 =
* Mejorada recepción de datos para comprobar envíos vacíos.

= 0.9 =
* Versión inicial.
