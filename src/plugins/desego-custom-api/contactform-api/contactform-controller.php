<?php

class Desego_Contactform_Controller extends WP_REST_Controller {

    public function register_routes() {
        $namespace = 'wc-desego/v1';
        $path= 'contactform/(?P<form_id>\d+)';
    
        register_rest_route( $namespace, '/' . $path, [
            array(
                'methods'             => 'POST',
                'callback'            => array( $this, 'send_form' ),
                'args' => array(
                    'form_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    )
                ),
                'permission_callback' => array( $this, 'send_form_permissions_check' )
            ),
      
        ]);     
    }
    
    public function send_form_permissions_check($request) {
        return current_user_can( 'edit_others_posts' );
    }

    public function send_form($request){
               
        $item = wpcf7_contact_form( 16118 );

        if ( ! $item ) {
            return new WP_Error( 'wpcf7_not_found',
                __( "The requested contact form was not found.", 'contact-form-7' ),
                array( 'status' => 404 ) );
        }

        //remove oauth request keys
        $valid_params = $request->get_params();

        unset($valid_params['form_id']);
        unset($valid_params['oauth_consumer_key']);
        unset($valid_params['oauth_nonce']);
        unset($valid_params['oauth_signature']);
        unset($valid_params['oauth_signature_method']);
        unset($valid_params['oauth_timestamp']);

        $_POST = $valid_params;

        $result = $item->submit();

        $response = array(
            'status' => $result['status'],
            'message' => $result['message'],
        );

        if ( 'validation_failed' == $result['status'] ) {
            $invalid_fields = array();

            foreach ( (array) $result['invalid_fields'] as $name => $field ) {
                $invalid_fields[] = array(
                    'into' => sanitize_html_class( $name ),
                    'message' => $field['reason']
                );
            }

            $response['invalidFields'] = $invalid_fields;
        }
 
        $response = new WP_REST_Response($response);
        $response->set_status(200);

        return $response;
    }
}