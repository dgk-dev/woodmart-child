<?php
//clases para usar controller pattern en API
require('contactform-controller.php');



add_action('rest_api_init', function(){
    //load area controller
    $desego_contactform_controller = new Desego_Contactform_Controller();
    $desego_contactform_controller->register_routes();

});