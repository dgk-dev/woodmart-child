<?php
//Agrega método para excluir categorías al endpoint de productos de woocommerce
//deprecated: use desego_custom_attributes_filter
add_filter('woocommerce_rest_product_object_query','desego_exclude_products_by_attribute', 10, 2);
function desego_exclude_products_by_attribute($args, $request){
    $tax_query = array();
    if(!empty($request['attribute'])&&!empty($request['attribute_term__not'])){
        $tax_query[] = array(
            'taxonomy' => $request['attribute'],
            'field'    => 'term_id',
            'terms'    => explode(',', $request['attribute_term__not']),
            'operator' => 'NOT IN'
        );
    }
    $args['tax_query'] = !empty($args['tax_query']) ? array_merge( $tax_query, $args['tax_query'] ) : $tax_query;
    return $args;
}

//Agrega parámetro a productos para ordenarlos de forma aleatoria
add_filter( 'rest_product_collection_params', 'desego_add_rand_product_orderby' );
function desego_add_rand_product_orderby( $query_params ) {
    $query_params['orderby']['enum'][] = 'rand';
    $query_params['orderby']['enum'][] = 'meta_value';
    $query_params['orderby']['enum'][] = 'meta_value_num';
	return $query_params;
}

//temporal fix, meta_value a meta_value_num solo en productos
add_filter('woocommerce_rest_product_object_query','desego_override_meta_value', 10, 2);
function desego_override_meta_value($args, $request){
    // equipment_module=true&orderby=meta_value
    if($request->get_param('equipment_module') && $request->get_param('orderby') == 'meta_value'){
        $request->set_param('orderby', 'meta_value_num');
    }
    return $args;
}

//Agrega método para mostrar reactivos relacionados a las opciones elegidas por el usuario en la parte de "mis equipos"
add_filter('woocommerce_rest_product_object_query','desego_related_reactives', 10, 2);
function desego_related_reactives($args, $request){
    if(!empty($request['interests'])){
        $tax_query = array();
        //solo reactivos
        $tax_query[] = array(
            'taxonomy' => 'pa_tipo',
            'field'    => 'slug',
            'terms'    => 'reactivos', //solo reactivos al consultar endpoint para reactivos relacionados
            'operator' => 'IN',
        );
        //categoria de reactivos
        $tax_query[] = array(
            'taxonomy' => 'pa_categoria',
            'field'    => 'term_id',
            'terms'    => explode(',', $request['interests']),
            'operator' => 'IN',
        );
        $args['tax_query'] = !empty($args['tax_query']) ? array_merge( $tax_query, $args['tax_query'] ) : $tax_query;
    }
    return $args;
}

// Excluye producto por puntos de la API, excepto cuando se llama específicamente o se hace un include
add_filter('woocommerce_rest_product_object_query','desego_remove_product_by_points_api', 10, 2);
function desego_remove_product_by_points_api($args, $request){
    $category_products_by_points = $request->get_param('category') && in_array(6950, $request->get_param('category'));
    $has_include_param = $request->get_param('include') && !empty($request->get_param('include'));
    
    if(!$category_products_by_points && !$has_include_param){
        $tax_query = array(
            array(
            'taxonomy' => 'product_cat',
            'field'    => 'slug',
            'terms'    => array('producto-por-puntos'),
            'operator' => 'NOT IN'
        ));
        $args['tax_query'] = !empty($args['tax_query']) ? array_merge( $tax_query, $args['tax_query'] ) : $tax_query;
    }
    return $args;
}

// Excluye productos por puntos de los reportes de la API. Usado en wc/v3/reports/top_sellers
add_filter('woocommerce_reports_get_order_report_data_args','desego_remove_product_by_points_api_reports', 10);
function desego_remove_product_by_points_api_reports($args){
    if(defined( 'REST_REQUEST' ) && REST_REQUEST){
        $where_meta = array(
            array(
                'meta_key'      => '_payment_method',
                'meta_value'    => 'reward_gateway',
                'operator'      => '!=',
            )
        );
        $args['where_meta'] = !empty($args['where_meta']) ? array_merge( $where_meta, $args['where_meta'] ) : $where_meta;
    }
    return $args;
}

// Revisar stock de los productos de la orden antes de crearla con la API
add_action( 'woocommerce_rest_pre_insert_shop_order_object', 'desego_order_check_stock', 1, 3 ); 
function desego_order_check_stock( $order, $request, $creating ) { 
    $line_items = $request->get_param('line_items');
    $no_stock = array();
    foreach($line_items as $line_item){
        $product = wc_get_product($line_item['product_id']);
        if(!$product) return new WP_Error('no_product', 'No existe un artículo con el siguiente ID', array('product_id' => $line_item['product_id']));
        if($product->get_manage_stock()){
            $quantity = $line_item['quantity'];
            $stock = $product->get_stock_quantity();
            $quantity > $stock ? $no_stock[] = array('product_id' => $product->get_id(), 'name' => $product->get_name()) : null;
        }
    }
    
    if(!empty($no_stock)) return new WP_Error('no_stock', 'No hay la existencia requerida para los siguientes artículos', $no_stock);

    return $order;   
}

//Agrega filtros por atributos para productos
add_filter('woocommerce_rest_product_object_query','desego_custom_attributes_filter', 10, 2);
function desego_custom_attributes_filter($args, $request){
    $tax_query = array();
    if(!empty($request['pa'])){
        foreach($request['pa'] as $key => $value){
            $tax_query[] = array(
                'taxonomy' => 'pa_'.$key,
                'field'    => 'slug',
                'terms'    => explode(',', $value),
                'operator' => 'IN'
            );
        }
    }

    if(!empty($request['pa__not'])){
        foreach($request['pa__not'] as $key => $value){
            $tax_query[] = array(
                'taxonomy' => 'pa_'.$key,
                'field'    => 'slug',
                'terms'    => explode(',', $value),
                'operator' => 'NOT IN'
            );
        }
    }
    $args['tax_query'] = !empty($args['tax_query']) ? array_merge( $tax_query, $args['tax_query'] ) : $tax_query;
    return $args;
}


// Agrega id de atributos en la respuesta de productos
add_filter( 'woocommerce_get_product_terms', 'desego_add_term_data', 5, 4 ); 
function desego_add_term_data( $terms, $product_id, $taxonomy, $args ) {
    if(defined( 'REST_REQUEST' ) && REST_REQUEST){
        $new_terms = array();
        foreach($terms as $term){
            $term_obj = get_term_by('name', $term, $taxonomy);
            $new_terms[] = array(
                "id" => $term_obj->term_id,
                "name" => $term_obj->name
            );
        }
        $terms = !empty($new_terms) ? $new_terms : $terms;
    }
    return $terms; 
}

// Cambiar url de imágenes por tamaños menores predefinidos en wordpress
add_filter("woocommerce_rest_prepare_product_object", "desego_change_images_response", 10, 3);
function desego_change_images_response($response, $post, $request) {
    if (empty($response->data)) return $response;

    foreach ($response->data['images'] as $key => $image) {
        $image_info = wp_get_attachment_image_src($image['id'], 'shop_single');
        $response->data['images'][$key]['src'] = $image_info[0];
    }
    
    return $response;
}

//Agrega filtros por propiedades de productos

// Primero expone las variables para la rest API
add_filter ('rest_query_vars', 'desego_add_meta_vars', 1);
function desego_add_meta_vars ($current_vars) {
    $current_vars = array_merge ($current_vars, array ('meta_key', 'meta_value'));
    return $current_vars;
}

// Luego agrega los filtros
add_filter('woocommerce_rest_product_object_query','desego_custom_meta_filter', 1, 2);
function desego_custom_meta_filter($args, $request){
    $meta_query = array();
    if(isset($request['equipment_module']) && $request['equipment_module']){
        $meta_query[] = array(
            'key'     => 'in_equipment_module',
            'value'   => 'yes',
        );
        $args['meta_key'] = 'equipment_module_order';        
    }
    $args['meta_query'] = !empty($args['meta_query']) ? array_merge( $meta_query, $args['meta_query'] ) : $meta_query;
    return $args;
}

// Pone "set_paid" siempre como true al crear una orden
// add_filter('woocommerce_rest_pre_insert_shop_order_object','desego_order_set_paid', 10, 3);
// function desego_order_set_paid($order, $request, $creating){
// 	$request->set_param('set_paid', true);
// 	return $order;
// }