<?php

class Desego_Related_Products_Controller extends WC_REST_Products_Controller {

    public function register_routes() {
        $namespace = 'wc-desego/v1';
        $path= 'products/related/(?P<user_id>\d+)';
    
        register_rest_route( $namespace, '/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_items' ),
                'args' => array(
                    'user_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            ),
        ]);     
    }

    protected function prepare_objects_query( $request ) {
        $args = WC_REST_CRUD_Controller::prepare_objects_query( $request );
        global $wpdb;
        $user_id = $request['user_id'];
        $table_name = $wpdb->prefix . "desego_equipment";
        $registry = $wpdb->get_row( "SELECT * FROM $table_name WHERE user_id = $user_id");

        // $selected_area = ($registry && $registry->area) ? $registry->area : 0;
        $selected_equipment = ($registry && $registry->equipment) ? json_decode($registry->equipment) : array();
        $selected_interests = ($registry && $registry->interests) ? json_decode($registry->interests) : array();

        $tax_query = array();

        // filtrar solo reactivos: atributo y categoria
        $tax_query[] = array(
            array(
                'relation' => 'OR',
                array(
                    'taxonomy' => 'pa_tipo',
                    'field'    => 'slug',
                    'terms'    => 'reactivos',
                    'operator' => 'IN',
                    
                ),
                array(
                    'taxonomy' => 'product_cat',
                    'field'    => 'slug',
                    'terms'    => 'reactivos',
                    'operator' => 'IN',
                )
            )
        );

        // filtrar área
        // if($selected_area){
        //     $area = $selected_area == 1 ? 'humano' : 'veterinario';
        //     $tax_query[] = array(
        //         'taxonomy' => 'pa_area',
        //         'field'    => 'slug',
        //         'terms'    => $area,
        //         'operator' => 'IN',
        //     );
        // }

        // filtrar líneas de interés
        if(!empty($selected_interests)){
            $tax_query[] = array(
                'taxonomy' => 'pa_categoria',
                'terms'    => $selected_interests,
                'operator' => 'IN'
            );
        }

        $args['tax_query'] = $tax_query;

        // filtrar (include) productos relacionados
        if(!empty($selected_equipment)){
            $related_products = array();
            foreach($selected_equipment as $equipment_id){
                $this_related_products = get_post_meta( $equipment_id, '_crosssell_ids', true);
                $related_products = !empty($this_related_products) ? array_merge($related_products, $this_related_products) : $related_products;
            }
            if(!empty($related_products)){
                $tax_products_ids = array();

                if(!empty($selected_interests)){
                    $tax_products_args = $args;
                    $tax_products_args['fields'] = 'ids';
                    $tax_products_args['posts_per_page'] = 100;
                    $tax_products_ids = get_posts( $tax_products_args );
                }

                $all_products = !empty($tax_products_ids) ? array_merge($related_products, $tax_products_ids) : $related_products;
                $args['post__in'] = $all_products;
                $args['orderby'] = 'post__in';
                $args['tax_query'] = array();
            }
        }

		return $args;
	}
}


class Desego_Recent_Products_Controller extends WC_REST_Products_Controller {

    public function register_routes() {
        $namespace = 'wc-desego/v1';
        $path= 'products/recent/(?P<user_id>\d+)';
    
        register_rest_route( $namespace, '/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_items' ),
                'args' => array(
                    'user_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            ),
        ]);     
    }

    protected function prepare_objects_query( $request ) {
        $args = WC_REST_CRUD_Controller::prepare_objects_query( $request );
        $customer_orders = get_posts( array(
            'numberposts' => 3,
            'meta_key'    => '_customer_user',
            'meta_value'  => $request['user_id'],
            'post_type'   => wc_get_order_types(),
            'post_status' => array_keys( wc_get_is_paid_statuses() ),
            'orderby' => 'date',
            'order' => 'desc'
        ) );
       
        // LOOP THROUGH ORDERS AND GET PRODUCT IDS
        $product_ids = array();
        foreach ( $customer_orders as $customer_order ) {
            $order = wc_get_order( $customer_order->ID );
            foreach ( $order->get_items() as $item ) {
                $product_ids[] = $item->get_product_id();
            }
        }
        $args['post__in'] = $product_ids;
        $args['orderby'] = 'post__in';
        $args['tax_query'] = array();
		return $args;
	}
}