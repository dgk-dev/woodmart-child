<?php

//Carga de controlador y rutas de productos
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    add_action('woocommerce_loaded' , 'desego_load_products_controller');
}

function desego_load_products_controller(){
    require('products-controller.php');
    add_action('rest_api_init', function(){
        $desego_related_products_controller = new Desego_Related_Products_Controller();
        $desego_related_products_controller->register_routes();
        $desego_recent_products_controller = new Desego_Recent_Products_Controller();
        $desego_recent_products_controller->register_routes();
        
    });
}