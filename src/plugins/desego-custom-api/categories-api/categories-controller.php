<?php

class Desego_Categories_Controller extends WP_REST_Controller {

    public function register_routes() {
        $namespace = 'wc-desego/v1';
        $path= 'categories';
    
        //related
        register_rest_route( $namespace, '/' . $path . '/related/(?P<user_id>\d+)', [
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_related_items' ),
                'args' => array(
                    'user_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            ),
        ]);


        //menu
        register_rest_route( $namespace, '/' . $path . '/menu', [
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_menu_items' ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            ),
        ]); 
    }

    public function get_items_permissions_check($request) {
        return current_user_can( 'edit_others_posts' );
    }

    public function get_related_items($request){
        global $wpdb;
        
        $user_id = $request['user_id'];
        $table_name = $wpdb->prefix . "desego_equipment";
        $registry = $wpdb->get_row( "SELECT * FROM $table_name WHERE user_id = $user_id");

        // $selected_area = ($registry && $registry->area) ? $registry->area : 0;
        $selected_equipment = ($registry && $registry->equipment) ? json_decode($registry->equipment) : array();
        $selected_interests = ($registry && $registry->interests) ? json_decode($registry->interests) : array();

        $categories_ids = array(); //push $selected_interests at the end

        if(!empty($selected_equipment)){
            $related_products = array();
            foreach($selected_equipment as $equipment_id){
                $this_related_products = get_post_meta( $equipment_id, '_crosssell_ids', true);
                $related_products = !empty($this_related_products) ? array_unique(array_merge($related_products, $this_related_products)) : $related_products;
            }
            if(!empty($related_products)){
                foreach($related_products as $product_id){
                    $product = wc_get_product( $product_id );
                    $product_attributes = !empty($product) ? $product->get_attributes() : array();
                    if(isset($product_attributes['pa_categoria'])){
                        $product_categories_ids = $product_attributes['pa_categoria']->get_options();
                        $categories_ids = !empty($product_categories_ids) ? array_unique(array_merge($categories_ids, $product_categories_ids)) : $categories_ids;
                    }
                }
            }
        }
        
        $categories_ids = array_unique(array_merge($categories_ids, $selected_interests));
        
        $response = array();

        foreach($categories_ids as $category_id){
            $category_data = get_term_by('id', $category_id, 'pa_categoria');
            $response[] = array(
                'id' => $category_data->term_id,
                'name' => $category_data->name,
                'slug' => $category_data->slug
            );
        }

        $response = new WP_REST_Response($response);
        $response->set_status(200);

        return $response;
    }

    public function get_menu_items($request){
        $response = array();

        $term_ids = array( // product_cat
            6803,   //química clínica
            19,     //hematología,
            20,     //inmunoensayos
            6946,   //hplc
            6938,   //electrolitos
            96,     //gasometria
            17,     //coagulación
            21,     //microelisa
            38,     //orina
            6804,   //veterinaria
            42,     //microscopios
            44,     //centrífugas
            99,     //kitlab premium
            45,     //equipo básico
            46,     //micropipetas
            47,     //material de laboratorio
            49,     //otros
            53,     //microbiología
            54,     //banco de sangre
            55,     //espermatobioscopía
            57      //reactivos
        );

        foreach($term_ids as $term_id){
            $term = get_term_by('id', $term_id, 'product_cat');
            $response[]= array(
                'id' => $term->term_id,
                'name' => $term->name,
                'slug' => $term->slug
            );
        }
        
        $response = new WP_REST_Response($response);
        $response->set_status(200);

        return $response;
    }
}