<?php

//Carga de controlador y rutas de categorías
require('categories-controller.php');

add_action('rest_api_init', function(){
    //load categories controller
    $desego_categories_controller = new Desego_Categories_Controller();
    $desego_categories_controller->register_routes();
});