<?php

//Carga de controlador y rutas de wishlist
require('wishlist-controller.php');

add_action('rest_api_init', function(){
    //load wishlist controller
    $desego_wishlist_controller = new Desego_Wishlist_Controller();
    $desego_wishlist_controller->register_routes();
});