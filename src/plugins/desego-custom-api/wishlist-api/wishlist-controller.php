<?php

if(!class_exists('Wishlist')) return;

use XTS\WC_Wishlist\Wishlist;

class Desego_Wishlist_Controller extends WP_REST_Controller {

    public function register_routes() {
        $namespace = 'wc-desego/v1';
        $path= 'wishlist/(?P<user_id>\d+)';
    
        register_rest_route( $namespace, '/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_items' ),
                'args' => array(
                    'user_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            ),
            array(
                'methods'             => 'POST',
                'callback'            => array( $this, 'update_item' ),
                'args' => array(
                    'user_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                    'product_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                ),
                'permission_callback' => array( $this, 'update_item_permissions_check' )
            ),
            array(
                'methods'             => 'DELETE',
                'callback'            => array( $this, 'delete_item' ),
                'args' => array(
                    'user_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                    'product_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                ),
                'permission_callback' => array( $this, 'delete_item_permissions_check' )
            ),
        ]);     
    }

    public function get_items_permissions_check($request) {
        return current_user_can( 'edit_others_posts' );
    }

    public function get_items($request){
        $wishlist = new Wishlist(false, $request['user_id']);

        $products = $wishlist->get_all();
 
        if(!$products) return new WP_Error( 'empty_wishlist', 'el usuario no tiene productos favoritos', array('status' => 404) );

       
        foreach($products as $key => $product){
            $products[$key]['ID'] = $products[$key]['product_id'];
        }

        $response = new WP_REST_Response($products);
        $response->set_status(200);

        return $response;
    }
    
    public function update_item_permissions_check($request) {
        return current_user_can( 'edit_others_posts' );
    }

    public function update_item($request){
        $product_id = $request['product_id'];
        if(!$product_id) return new WP_Error( 'empty_product', 'parámetro product_id vacio.', array('status' => 400) );

        $wishlist = new Wishlist(false, $request['user_id']);

        $add = $wishlist->add($product_id);

        if(!$add) return new WP_Error( 'add_error', 'ocurrió un error al dar de alta el producto.', array('status' => 500) );

        $response = new WP_REST_Response('success');
        $response->set_status(200);

        return $response;
    }
    
    public function delete_item_permissions_check($request) {
        return current_user_can( 'edit_others_posts' );
    }

    public function delete_item($request){
        $product_id = $request['product_id'];
        if(!$product_id) return new WP_Error( 'empty_product', 'parámetro product_id vacio.', array('status' => 400) );

        $wishlist = new Wishlist(false, $request['user_id']);

        $remove = $wishlist->remove($product_id);;

        if(!$remove) return new WP_Error( 'remove_error', 'ocurrió un error al dar de baja el producto.', array('status' => 500) );

        $response = new WP_REST_Response('success');
        $response->set_status(200);

        return $response;
    }
}