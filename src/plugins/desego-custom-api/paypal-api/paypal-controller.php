<?php

class Desego_Paypal_Controller extends WP_REST_Controller {

    public function register_routes() {
        $namespace = 'wc-desego/v1';
        $path= 'paypal';
    
        register_rest_route( $namespace, '/' . $path.'/token', [
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_paypal_access_token' ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            )
        ]);     
    }

    public function get_items_permissions_check($request) {
        return current_user_can( 'edit_others_posts' );
    }

    public function get_paypal_access_token(){
        $ch = curl_init();
        $clientId = "ATBmbKkpDDj4t6IngyxCdw2EIGZzMWsYX3ZNpMMBZ_7EB35VIbJLsoxXNHl0TXqvYhXutDsmsk2iTKBG";
        $secret = "EFe94ho-Wf40sRFo8d6bgxGAQ1HtrijbMNxDb-47deJ-SS2F0p57-wVkxS2uQuPO1IFkr6jaJYC-qIBr";

        curl_setopt($ch, CURLOPT_URL, "https://api.paypal.com/v1/oauth2/token");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLVERSION , 6);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

        $result = curl_exec($ch);
        curl_close($ch);

        if(empty($result)) return new WP_Error( 'token_error', 'se produjo un error al generar el token', array('status' => 400) );
  
        $response = new WP_REST_Response(json_decode($result));
        $response->set_status(200);

        return $response;
    }
}