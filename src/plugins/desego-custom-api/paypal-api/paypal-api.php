<?php

//Carga de controlador y rutas de paypal
require('paypal-controller.php');

add_action('rest_api_init', function(){
    //load paypal controller
    $desego_paypal_controller = new Desego_Paypal_Controller();
    $desego_paypal_controller->register_routes();
});