<?php
/**
* Plugin Name: Desego custom API
* Description: Endpoints personalizados para la app de desego.
* Version: 0.3
* Author: dgk
* Author URI: http://dgk.com.mx/
**/

//Cargar de API mis equipos
include(plugin_dir_path( __FILE__ ) . 'equipment-api/equipment-api.php');

//Carga de API wishlist
include(plugin_dir_path( __FILE__ ) . 'wishlist-api/wishlist-api.php');

//Carga de API revslider 
include(plugin_dir_path( __FILE__ ) . 'revslider-api/revslider-api.php');

//Carga de API para puntos
include(plugin_dir_path( __FILE__ ) . 'points-api/points-api.php');

//Carga de API de paypal
include(plugin_dir_path( __FILE__ ) . 'paypal-api/paypal-api.php');

//Carga de API de productos
include(plugin_dir_path( __FILE__ ) . 'products-api/products-api.php');

//Carga de API de categorias
include(plugin_dir_path( __FILE__ ) . 'categories-api/categories-api.php');

//Carga de API para formulario de contacto
include(plugin_dir_path( __FILE__ ) . 'contactform-api/contactform-api.php');

//Carga de API para envios
include(plugin_dir_path( __FILE__ ) . 'shipping-api/shipping-api.php');

//Carga de otras funciones 
include(plugin_dir_path( __FILE__ ) . 'includes/functions.php');