<?php

class Desego_Equipment_Interests_Controller extends WP_REST_Controller {

    public function register_routes() {
        $namespace = 'wc-desego/v1';
        $path= 'equipment/interests/(?P<user_id>\d+)';
    
        register_rest_route( $namespace, '/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_items' ),
                'args' => array(
                    'user_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            ),
            array(
                'methods'             => 'POST',
                'callback'            => array( $this, 'update_item' ),
                'args' => array(
                    'user_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                    'interests_ids' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            $ids= json_decode(stripslashes($param));
                            if(json_last_error() != JSON_ERROR_NONE) return false;
                            return ctype_digit(implode('',$ids)) || empty($ids);

                        }
                    ),
                ),
                'permission_callback' => array( $this, 'update_item_permissions_check' )
            ),
        ]);     
        register_rest_route( $namespace, '/' . 'equipment/interests/all', [
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_interests' ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            )
        ]);     
    }

    public function get_items_permissions_check($request) {
        return current_user_can( 'edit_others_posts' );
    }

    public function get_items($request){
        global $wpdb;
        $user_id = $request['user_id'];
        $table_name = $wpdb->prefix . "desego_equipment";
        $query = $wpdb->get_row( "SELECT interests FROM $table_name WHERE user_id = $user_id");

        if (empty($query) || !$query->interests) {
            return new WP_Error( 'empty_interests', 'el usuario no tiene equipo seleccionado', array('status' => 404) );
        }

        $response = new WP_REST_Response(json_decode($query->interests));
        $response->set_status(200);

        return $response;
    }
    
    public function update_item_permissions_check($request) {
        return current_user_can( 'edit_others_posts' );
    }

    public function update_item($request){
        global $wpdb;
        $interests_ids = str_replace('"','',$request['interests_ids']);
        $user_id = $request['user_id'];
        $table_name = $wpdb->prefix . 'desego_equipment';
        $success = 0;
        $registry = $wpdb->get_row( "SELECT * FROM $table_name WHERE user_id = $user_id");
        $now = date("Y-m-d H:i:s");

        if(!$registry){
            $success = $wpdb->insert( 
                $table_name, 
                array(
                    'user_id' => $user_id, 
                    'interests' => $interests_ids,
                    'created' => $now,
                    'updated' => $now,
                ), 
                array( 
                    '%d',
                    '%s',
                ) 
            );
        }else{
            $success = $wpdb->update( 
                $table_name, 
                array( 
                    'interests' => $interests_ids,
                    'updated' => $now,
                ), 
                array( 'id' => $registry->id ), 
                array( 
                    '%s',
                    '%s'
                ),
                array( '%d' ) 
            );
        }

        if (!$success) {
            return new WP_Error( 'database_write', 'no se pudo escribir en la base de datos', array('status' => 500) );
        }

        $response = new WP_REST_Response('success');
        $response->set_status(200);

        return $response;
    }

    public function get_interests($request){
        $product_ids = get_posts(array(
            'post_type'      => array('product'),
            'posts_per_page' => -1,
            'tax_query'      => array(
                'relation' => 'OR',
                array(
                    'taxonomy'        => 'pa_tipo',
                    'field'           => 'slug',
                    'terms'           =>  array('reactivos'),
                    'operator'        => 'IN',
                ),
                array(
                    'taxonomy'        => 'product_cat',
                    'field'           => 'slug',
                    'terms'           =>  array('reactivos'),
                    'operator'        => 'IN',
                ),
            ),
            'fields' => 'ids'
        ));
        
        $terms = get_terms(array(
            'taxonomy' => 'pa_categoria',
            'object_ids' => $product_ids,
            'fields' => 'ids'
        ));
        $rest_request = new WP_REST_Request( 'GET', '/wc/v3/products/attributes/7/terms' );
        $rest_request->set_query_params( [ 'include' => implode(',',$terms), 'per_page' => '100' ] );
        $rest_response = rest_do_request( $rest_request );
        $server = rest_get_server();
        $term_response = $server->response_to_data( $rest_response, false );
        
        $response = new WP_REST_Response($term_response);
        $response->set_status(200);

        return $response;
    }
}