<?php
//clases para usar controller pattern en API
require('area-controller.php');
require('equipment-controller.php');
require('interests-controller.php');


add_action('rest_api_init', function(){
    //load area controller
    $desego_equipment_area_controller = new Desego_Equipment_Area_Controller();
    $desego_equipment_area_controller->register_routes();

    //load equipment controller
    $desego_equipment_controller = new Desego_Equipment_Controller();
    $desego_equipment_controller->register_routes();

    //load interests controller
    $desego_equipment_interests_controller = new Desego_Equipment_Interests_Controller();
    $desego_equipment_interests_controller->register_routes();
});

