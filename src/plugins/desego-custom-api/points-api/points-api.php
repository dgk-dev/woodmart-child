<?php

//Carga de controlador y rutas de puntos
require('points-controller.php');

add_action('rest_api_init', function(){
    //load points controller
    $desego_points_controller = new Desego_Points_Controller();
    $desego_points_controller->register_routes();
});

//Cuando se hace una orden en la api con el gateway "reward_gateway" revisa los puntos, los descuenta si el puntaje es correcto, si no pone la orden como fallida. Para evitar que una orden caiga en fallida hay que revisar primero los puntos en la app
add_action( 'woocommerce_payment_complete', 'api_set_points',  1, 1  );
function api_set_points($order_id){
    $is_rest_request = defined( 'REST_REQUEST' ) ? constant( 'REST_REQUEST' ) : false;
    if ($is_rest_request && get_post_meta( $order_id , '_payment_method' , true ) == 'reward_gateway' && class_exists( 'RS_Points_Data' )){
        $order = new WC_Order( $order_id );
        $orderObj    = srp_order_obj( $order );
        $user_id = $orderObj['order_userid'];
        $PointsData = new RS_Points_Data($user_id);
        $total_points     = $PointsData->total_available_points();
        $redeemedpoints = gateway_points( $order_id );

        if($total_points < $redeemedpoints){
            $order_status = 'failed';
        }else{
            update_post_meta( $order_id , 'total_redeem_points_for_order_point_price' , $redeemedpoints );
            update_post_meta( $order_id , 'frontendorder' , 1 );
    
            $order->payment_complete();
            $order_status = get_option( 'rs_order_status_after_gateway_purchase' );
        }

        $order->update_status( $order_status );
    }
}

/*
 * Add a reward_points field to the Order API response.
*/ 
function desego_wc_rest_reward_points_in_order_object( $response, $object, $request ) {
	// Get the value
	$reward_points_meta_field_cart = ( $value_cart = get_post_meta($object->get_id(), 'points_for_current_order_based_on_cart_total', true) ) ? $value_cart : 0;
    
    $reward_points_meta_field_products = ( $value_products = get_post_meta($object->get_id(), 'rs_points_for_current_order_as_value', true) ) ? $value_products : 0;

	$response->data['reward_points'] = round_off_type($reward_points_meta_field_cart + $reward_points_meta_field_products);

	return $response;
}
add_filter( 'woocommerce_rest_prepare_shop_order_object', 'desego_wc_rest_reward_points_in_order_object', 10, 3 );


//Add rewards points to order when created through REST API
function desego_insert_reward_points_shop_order_object( $order ) {
    if (class_exists('RSProductPurchaseFrontend')){
        desego_save_points_info_in_order($order->get_id());
    }
}
add_action('woocommerce_rest_insert_shop_order_object', 'desego_insert_reward_points_shop_order_object', 10, 3);


function desego_save_points_info_in_order($order_id){
    if ( ! is_user_logged_in() )
        return ;
    $order = wc_get_order($order_id);
    $user_id = $order->get_user_id() ;

    if ( get_option( 'rs_enable_product_category_level_for_product_purchase' ) == 'yes' ) {
        $PointInfo = desego_original_points_for_product($order_id) ;
        if ( srp_check_is_array( $PointInfo ) ) {
            update_post_meta( $order_id , 'points_for_current_order' , $PointInfo ) ;
            $Points = array_sum( $PointInfo ) ;
            $Points = RSMemberFunction::earn_points_percentage( $user_id , ( float ) $Points ) ;
            update_post_meta( $order_id , 'rs_points_for_current_order_as_value' , $Points ) ;
        }
    } else {
        if ( get_option( 'rs_award_points_for_cart_or_product_total' ) == '1' ) {
            $PointInfo = desego_original_points_for_product($order_id) ;
            if ( srp_check_is_array( $PointInfo ) ) {
                update_post_meta( $order_id , 'points_for_current_order' , $PointInfo ) ;
                $Points = array_sum( $PointInfo ) ;
                $Points = RSMemberFunction::earn_points_percentage( $user_id , ( float ) $Points ) ;
                update_post_meta( $order_id , 'rs_points_for_current_order_as_value' , $Points ) ;
            }
        } else {
            $Order           = new WC_Order( $order_id ) ;
            $CartTotalPoints = get_reward_points_based_on_cart_total( $Order->get_total() ) ;
            $CartTotalPoints = RSMemberFunction::earn_points_percentage( $user_id , ( float ) $CartTotalPoints ) ;
            if ( ! empty( $CartTotalPoints ) )
                update_post_meta( $order_id , 'points_for_current_order_based_on_cart_total' , $CartTotalPoints ) ;
        }
    }
    update_post_meta( $order_id , 'frontendorder' , 1 ) ;
}

function desego_original_points_for_product($order_id){
    $order = wc_get_order($order_id);
    $user_id = $order->get_user_id() ;
    if ( check_banning_type( $user_id ) == 'earningonly' || check_banning_type( $user_id ) == 'both' )
        return array() ;

    global $totalrewardpoints ;
    $points = array() ;
    foreach ( $order->get_items() as $value ) {
        if ( block_points_for_salepriced_product( $value->get_product_id() , $value->get_variation_id() ) == 'yes' )
            continue ;
        $args                 = array(
            'productid'   => $value->get_product_id() ,
            'variationid' => $value->get_variation_id() ,
            'item'        => $value ,
                ) ;
        $Points               = check_level_of_enable_reward_point( $args ) ;
        $user_role_percentage = RSMemberFunction::earn_points_percentage( $user_id , ( float ) $Points ) ;
        if ( empty( $user_role_percentage ) )
            continue ;

        $totalrewardpoints = $Points ;
        $ProductId         = ! empty( $value->get_variation_id() ) ? $value->get_variation_id() : $value->get_product_id() ;

        if ( ! empty( $totalrewardpoints ) )
            $points[ $ProductId ] = $Points ;
    }
    return $points ;
}

add_filter('woocommerce_is_purchasable', 'desego_points_product_is_purchasable', 25, 2);
function desego_points_product_is_purchasable( $is_purchasable, $product ) {

    $is_rest_request = defined( 'REST_REQUEST' ) ? constant( 'REST_REQUEST' ) : false;

    if($is_rest_request){
        $is_purchasable = has_term( array( 'product-por-puntos' ), 'product_cat', $product->get_id() ) || $is_purchasable;
    }

    return $is_purchasable;
    
}