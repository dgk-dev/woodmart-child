<?php

class Desego_Points_Controller extends WP_REST_Controller {

    public function register_routes() {
        $namespace = 'wc-desego/v1';
        $path= 'points/(?P<user_id>\d+)';
    
        register_rest_route( $namespace, '/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_items' ),
                'args' => array(
                    'user_id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            )
        ]);     
    }

    public function get_items_permissions_check($request) {
        return current_user_can( 'edit_others_posts' );
    }

    public function get_items($request){
        $user_id = $request['user_id'];
        if(!class_exists( 'RS_Points_Data' )){
            return new WP_Error( 'plugin_error', 'el plugin de puntos no esta activado', array('status' => 404) );
        }
        
        $PointsData = new RS_Points_Data($user_id) ;
        $total_points = $PointsData->total_available_points() ;
            
        $response = new WP_REST_Response(strval(round($total_points,1)));
        $response->set_status(200);

        return $response;
    }
    
}