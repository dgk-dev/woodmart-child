<?php

//Carga de controlador y rutas de categorías
require('shipping-controller.php');

add_action('rest_api_init', function(){
    //load shipping controller
    $desego_shipping_controller = new Desego_Shipping_Controller();
    $desego_shipping_controller->register_routes();
}, 20);