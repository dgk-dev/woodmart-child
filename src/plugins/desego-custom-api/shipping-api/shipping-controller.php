<?php

class Desego_Shipping_Controller extends WP_REST_Controller {

    public function register_routes() {
        $namespace = 'wc-desego/v1';
        $path= 'shipping/methods';
    
        register_rest_route( $namespace, '/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_items' ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            ),
        ]); 
    }

    public function get_items_permissions_check($request) {
        return current_user_can( 'edit_others_posts' );
    }

    public function get_items($request){
        
        $request = new WP_REST_Request( 'GET', '/wc/v3/shipping/zones/2/methods' );
        $response = rest_do_request( $request );
        $server = rest_get_server();
        $shipping_methods = $server->response_to_data( $response, false );

        $shipping_methods_response = array();
        foreach($shipping_methods as $method){
            if($method['method_id'] == 'free_shipping'){
                $shipping_methods_response['free_shipping'] = array(
                    'min_amount' => $method['settings']['min_amount']['value']
                );
            }elseif($method['method_id'] == 'flat_rate'){
                $shipping_methods_response['flat_rate'] = array(
                    'cost' => $method['settings']['cost']['value']
                );
            }
        }

        $response = new WP_REST_Response($shipping_methods_response);
        $response->set_status(200);

        return $response;
    }
}