(function($) {
	$(document).ready( function() {
        var $mainContainer = $('#insertos_posts_wrap.insertos-wrap');
        if(!$mainContainer.length) return;
        var $siteContent = $('.site-content');
        var currentXhr;

		function get_posts($params) {
			currentXhr = $.ajax({
				url : insertosGlobalObj.ajaxurl,
				data: {
					action: 'insertos_filter',
					params: $params,
				},
				dataType : 'json', // this data type allows us to receive objects from the server
				type : 'POST',
				beforeSend : function(){
					$mainContainer.empty();
					$siteContent.addClass('ajax-loading');
				},
				complete : function(){
					$siteContent.removeClass('ajax-loading');
				},
				error : function(xhr, status, error) {
					console.log('status', xhr.status);
					console.log('status', status);
					console.log('error', error);
					console.log('message', xhr.responseText);
					$mainContainer.empty();
					$mainContainer.html('<p>Ha ocurrido un error</p>');
				},
				success : function( data ){
	 
					// when filter applied:
					// set the current page to 1
					insertosGlobalObj.current_page = 1;
	 
					// set the new query parameters
					insertosGlobalObj.posts = data.posts;
	 
					// set the new max page parameter
					insertosGlobalObj.max_page = data.max_page;
	 
					// insert the posts to the container
					$mainContainer.html(data.content);
					
				}
			});		
		}

		/**
		 * Bind get_posts to tag cloud and navigation
		 */
		$('.sc-ajax-filter-multi').on('click', 'a[data-filter], .pagination a', function(event) {
			if(event.preventDefault) { event.preventDefault(); }
            currentXhr && currentXhr.readyState != 4 && currentXhr.abort();
			var $this = $(this);

			/**
			 * Set filter active
			 */
			if ($this.data('filter')) {
				var $page = 1;

				/**
				 * If all terms, then deactivate all other
				 */
				$this.closest('ul').find('.active').removeClass('active chosen');
				

				// Toggle current active
				$this.parent('li').toggleClass('active chosen');
					
				/**
				 * Get All Active Terms
				 */
				var $active = {};
				var $terms  = $this.closest('ul').find('.active');

				if ($terms.length) {
					$.each($terms, function(index, term) {
						
						var $a    = $(term).find('a');
						var $tax  = $a.data('filter');
						var $slug = $a.data('term');

						if ($tax in $active) {
							$active[$tax].push($slug);
						}
						else {
							$active[$tax]      = [];
							$active[$tax].push($slug);
						}						
					});
				} else {
					$('a[data-term="all-terms"]').trigger('click');
				}
				
                $params    = {
                    'page'  : $page,
                    'terms' : $active,
                    'qty'   : $this.closest('#container-async').data('paged'),
                };
    
                // Run query
                get_posts($params);
                $('.woodmart-close-side.woodmart-close-side-opened').click();
			}
		});


		/**
		 * Show all posts on page load
		 */
		$('a[data-term="all-terms"]').trigger('click');

	});

})(jQuery);