(function ($) {
    
    //cambio de margen en sliders owl-carousel
    $('.owl-carousel').each(function(i, item){
        change_owl_margin($(item));
    });

    function change_owl_margin($item){
        var carouselData = $item.data('owl.carousel');
        if(typeof carouselData !== 'undefined'){
            $item.data('owl.carousel').options.margin = 20;
            $item.data('owl.carousel').settings.margin = 20;
            $item.trigger('refresh.owl.carousel');
        }else{
            setTimeout(function(){change_owl_margin($item)}, 700);
        }
    }

    if(desegoGlobalObject.add_cart_button_script){
        // Update quantity on ‘a.button’ in ‘data-quantity’ attribute (for ajax)
        // Update quantity
        $(".add_to_cart_button.product_type_simple").on('click input', function() {
            $(this).data('quantity', $(this).parent().parent().find('input.qty').val() ); 
        });        

        // On "adding_to_cart" delegated event, removes others "view cart" buttons 
        $(document.body).on("adding_to_cart", function() {
            $(".added_to_cart").remove();
        });
    }

    //Buy buy_now_submit_form
    $('#buy_now_button').click(function(){
        // set value to 1
        $('#is_buy_now').val('1');
        //submit the form
        //$('form.cart').submit();
    });
})(jQuery);